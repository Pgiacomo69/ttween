unit mainform;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  System.Actions, Vcl.ActnList, Vcl.ComCtrls, Vcl.ToolWin,
  Vcl.ExtCtrls, System.ImageList, Vcl.ImgList, Vcl.StdCtrls,
  Vcl.Buttons,
  qtx.tween.core,
  qtx.tween.components;

type


  TClickEdit = class(TCustomPanel)
  type
    TClickEditState = (esNone, esTransit, esEdit);
  strict private
    FLabel:     TLabel;
    FEdit:      TEdit;
    FState:     TClickEditState;
  strict protected
    procedure   SetLabel(const NewLabel: TLabel); virtual;
    procedure   SetEdit(const NewEdit: TEdit); virtual;

    procedure   HandleLostEditFocus(sender: TObject); virtual;
    procedure   HandleLabelClicked(sender: TObject); virtual;
    procedure   HandleExit(sender: TObject); virtual;

    procedure   Resize; override;
  public
    property DockManager;

    procedure   ShowEdit;
    procedure   HideEdit;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property    Text: TLabel read FLabel write SetLabel;
    property    Edit: TEdit read FEdit write SetEdit;

    property Align;
    property Alignment;
    property Anchors;
    property AutoSize;
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    property BevelWidth;
    property BiDiMode;
    property BorderWidth;
    property BorderStyle;
    property Caption;
    property Color;
    property Constraints;
    property Ctl3D;
    property UseDockManager default True;
    property DockSite;
    property DoubleBuffered;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FullRepaint;
    property Font;
    property Locked;
    property Padding;
    property ParentBiDiMode;
    property ParentBackground;
    property ParentColor;
    property ParentCtl3D;
    property ParentDoubleBuffered;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowCaption;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Touch;
    property VerticalAlignment;
    property Visible;
    property StyleElements;
    property OnAlignInsertBefore;
    property OnAlignPosition;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGesture;
    property OnGetSiteInfo;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
  end;

  TForm1 = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    acHAnim: TAction;
    Panel1: TPanel;
    ImageList1: TImageList;
    BitBtn1: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    acVAnim: TAction;
    Button1: TButton;
    procedure acHAnimExecute(Sender: TObject);
    procedure acHAnimUpdate(Sender: TObject);
    procedure acVAnimExecute(Sender: TObject);
    procedure acVAnimUpdate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  LTextEdit: TClickEdit;
begin
  LTextEdit := TClickEdit.Create(TabSheet1);
  LtextEdit.Parent := TabSheet1;
  LTextEdit.SetBounds(10, 10, 200, 26);
end;

procedure TForm1.acVAnimUpdate(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    TAction(Sender).Enabled := not TweenControlLocked(Panel2);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  case Button1.Tag of
  0:  begin
        Button1.Tag := 1;
        Button1.GrowBy(40, 40, 0.2);
      end;
  else begin
        Button1.Tag := 0;
        Button1.ShrinkBy(40, 40, 0.2);
      end;
  end;
end;

procedure TForm1.acVAnimExecute(Sender: TObject);
begin
  if Panel2.height < 220 then
    Panel2.SizeBottom(220, 0.2)
  else
    Panel2.SizeBottom(41, 0.2);
end;

procedure TForm1.acHAnimUpdate(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    TAction(Sender).Enabled := not TweenControlLocked(Panel1);
end;

procedure TForm1.acHAnimExecute(Sender: TObject);
begin
  if Panel1.Width < 220 then
    Panel1.SizeRight(220, 0.2)
  else
    panel1.SizeRight(53, 0.2);
end;

{ TClickEdit }

constructor TClickEdit.Create(AOwner: TComponent);
begin
  inherited;

  FState := esNone;

  self.Padding.Left := 2;
  self.Padding.Top := 2;
  self.Padding.Right := 2;
  self.Padding.Bottom := 2;
  //self.BevelOuter := bvNone;

  self.TabStop := true;

  // Create edit first, behind label
  FEdit := TEdit.Create(self);
  FEdit.Parent := self;
  FEdit.Visible := false;
  FEdit.OnExit := HandleLostEditFocus;

  // Label has higher visibility than editbox
  FLabel := TLabel.Create(self);
  FLabel.Parent := self;
  FLabel.OnClick := HandleLabelClicked;
  FLabel.Caption := 'Click me';
  FLabel.Transparent := false;
  FLabel.Alignment := TAlignment.taCenter;
  FLabel.Layout := TTextLayout.tlCenter;

  self.OnExit := HandleExit;
end;

destructor TClickEdit.Destroy;
begin
  FLabel.Free;
  FEdit.Free;
  inherited;
end;

procedure TClickEdit.SetLabel(const NewLabel: TLabel);
begin
  FLabel.Assign(NewLabel);
end;

procedure TClickEdit.SetEdit(const NewEdit: TEdit);
begin
  FEdit.Assign(NewEdit);
end;

procedure TClickEdit.ShowEdit;
var
  LRect:  TRect;
begin
  if FState = esNone then
  begin
    if not (csDestroying in ComponentState) then
    begin
      FState := esTransit;

      LRect := ClientRect;
      AdjustClientRect(LRect);

      // Position edit
      FEdit.Top := LRect.Bottom;
      FEdit.Left := LRect.left;
      FEdit.Width := LRect.Width;
      FEdit.Height := LRect.Height;
      FEdit.Visible := true;

      // Slowly size the label away
      FLabel.MoveAndSize(LRect.Left, -LRect.height, LRect.Width, LRect.Height,0.3, TTweenEasingType.ttlinear,
      procedure ()
      begin
        FLabel.Visible := false;
      end);

      // Slowly  move the editbox into view
      FEdit.MoveAndSize(LRect.Left, LRect.top, LRect.Width, LRect.Height, 0.3, TTweenEasingType.ttlinear,
      procedure ()
      begin
        FState := esEdit;
        if not (csDestroying in ComponentState) then
        begin
          FEdit.SetFocus();
        end;
      end);
    end;
  end;
end;

procedure TClickEdit.HideEdit;
var
  LRect:  TRect;
begin
  if not (csDestroying in ComponentState) then
  begin
    if FState = esEdit then
    begin
      FState := esTransit;

      LRect := ClientRect;
      AdjustClientRect(LRect);

      // Position edit
      FLabel.Top := -LRect.Height;
      FLabel.Left := 0;
      FLabel.Width := LRect.Width;
      FLabel.Height := LRect.Height;
      FLabel.Visible := true;

      // Slowly size the label down
      FLabel.MoveAndSize(LRect.Left, LRect.Top, LRect.Width, LRect.height,0.3, TTweenEasingType.ttlinear,
      procedure ()
      begin
        FEdit.Visible := false;
      end);

      // Slowly  move the editbox into view
      FEdit.MoveAndSize(LRect.Left, LRect.top + LRect.height, LRect.Width, LRect.Height, 0.3, TTweenEasingType.ttlinear,
      procedure ()
      begin
        FState := esNone;
        FEdit.Visible := false;
      end);
    end;
  end;
end;

procedure TClickEdit.HandleExit(sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FState = esEdit then
      HandleLostEditFocus(Sender);
  end;
end;

procedure TClickEdit.HandleLostEditFocus(sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FState = esEdit then
      HideEdit();
  end;
end;

procedure TClickEdit.HandleLabelClicked(sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    ShowEdit();
end;

procedure TClickEdit.Resize;
var
  LRect:  TRect;
begin
  inherited;

  LRect := ClientRect;
  AdjustClientRect(LRect);

  case FState of
  esNone:
    begin
      //if FEdit.Visible then
      //  FEdit.Visible := false;
      FLabel.SetBounds(LRect.Left, LRect.Top, LRect.width, LRect.height);
    end;
  esTransit:
    begin
    end;
  esEdit:
    begin
      //if FLabel.Visible then
      //  FLabel.Visible := false;
      FEdit.SetBounds(LRect.Left, LRect.Top, LRect.width, LRect.height);
    end;
  end;
end;

end.
