unit qtx.tween.package;

// ############################################################################
// #
// # Quartex code library
// # ====================
// # Written by Jon L. Aasenden
// # Copyright Quartex Components LTD. All rights reserved.
// #
// # NOTE: This unit is not freeware. It has been released under Patreon,
// # which means that unless you have PAID A VALID TIER for this code,
// # you cannot use it in your projects.
// #
// # To obtain a valid support tier, please visit my Patreon page at:
// #    https://www.patreon.com/quartexnow
// #
// # Note: A valid Patreon tier for this code is very reasonable, ranging from
// #       USD $5 to $50. Please respect the time and effort that has gone
// #       into writing this source-code.
// #
// ############################################################################


interface

uses
  System.Classes,
  qtx.tween.core,
  qtx.tween.components;


procedure Register;



implementation

{$I 'qtx.tween.inc'}

uses
  qtx.system.dispatch;

procedure Register;
begin
  RegisterComponents('Tweening', [TTweenSizeAnimation, TTweenMoveAnimation, TTweenMoveSizeAnimation]);
end;

end.
