unit qtx.tween.core;

// ############################################################################
// #
// # Quartex code library
// # ====================
// # Written by Jon L. Aasenden
// # Copyright Quartex Components LTD. All rights reserved.
// #
// # NOTE: This unit is not freeware. It has been released under Patreon,
// # which means that unless you have PAID A VALID TIER for this code,
// # you cannot use it in your projects.
// #
// # To obtain a valid support tier, please visit my Patreon page at:
// #    https://www.patreon.com/quartexnow
// #
// # Note: A valid Patreon tier for this code is very reasonable, ranging from
// #       USD $5 to $50. Please respect the time and effort that has gone
// #       into writing this source-code.
// #
// ############################################################################

{$I 'qtx.tween.inc'}

{$M+}

interface

uses
  System.Types,
  System.SysUtils,
  System.Classes,
  System.DateUtils,
  System.Math,
  System.Generics.Collections,
  Winapi.Windows,
  Vcl.Controls,
  Vcl.Forms
  ;

const
  CND_DURATION_FACTOR = 1000;

type

  // Exception class for general TTween errors
  ETweenError   = class(Exception);
  ETweenElement = class(ETweenError);

  // Forward declarations
  TTweenInterfacedObject  = class;
  TTweenElement = class;
  TTweenEngine  = class;

  // The tween easing type:
  // ======================
  // The easing type represents the formula the tween-engine
  // applies to the values. Each results in very different behaviors.
  // For a visual representation, see: https://easings.net/nb
  TTweenEasingType = (
    ttlinear,
    ttQuadIn,
    ttQuadOut,
    ttQuadInOut,
    ttCubeIn,
    ttCubeOut,
    ttCubeInOut,
    ttQuartIn,
    ttQuartOut,
    ttQuartInOut,
    ttQuintIn,
    ttQuintOut,
    ttQuintInOut,
    ttSineIn,
    ttSineOut,
    ttSineInOut,
    ttExpoIn,
    ttExpoOut,
    ttExpoInOut
    );

  // The tween runtime behavior:
  // ===========================
  // The behavior type defines how the tween-engine should
  // execute the tween. Should it run just once? Should it
  // be repeated perpetually of a fixed number of times?
  // Or should it oscillate (go back and fourth)
  TTweenBehavior = (
    tbSingle,
    tbRepeat,
    tbOscillate
    );

  // The tween object state:
  // =======================
  // This framework supports multiple tween objects, or tasks,
  // that can run in parallell. Each can have different durations,
  // speeds, easing mode and runtime behavior.
  // These states defines if a tween is idle (not started)
  // running, paused or finished (done).
  TTweenState = (
    tsIdle,
    tsRunning,
    tsPaused,
    tsDone
    );

  TTweenUpdatedEvent          = procedure (Sender: TObject) of object;
  TTweenStartedEvent          = procedure (sender: TObject) of object;
  TTweenFinishedEvent         = procedure (sender: TObject) of object;
  TTweenFinishedPartialEvent  = procedure (sender: TObject) of object;
  TTweenItemResumedEvent      = procedure (const Sender: TTweenElement) of object;
  TTweenItemPausedEvent       = procedure (const Sender: TTweenElement) of object;
  TTweenItemFinishedEvent     = procedure (const Sender: TTweenElement) of object;
  TTweenItemStartedEvent      = procedure (const Sender: TTweenElement) of object;
  TTweenItemUpdatedEvent      = procedure (const Sender: TTweenElement) of object;


  TTweenEasingFunction  = function (t, b, c, d: double): double of object;

  ITweenElement = interface
    ['{130CEB67-2F3F-4D49-AC94-ABAB4552AB9D}']
    procedure   __SetStartTime(const NewStartTime: TDateTime);
    procedure   __SetStartValue(const NewStartValue: double);
    procedure   __SetDistance(const NewDistance: double);
    procedure   __SetDuration(const NewDuration: double);
    procedure   __SetEasing(const NewEasing: TTweenEasingType);
    procedure   __SetBehavior(const NewBehavior: TTweenBehavior);
    procedure   __SetState(const NewState: TTweenState);
    procedure   __SetValue(const NewValue: double);
    procedure   __Update(const NewValue: double);
  end;

  TTweenInterfacedObject = class(TPersistent)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    // Implements:: IUnknown
    function    QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function    _AddRef: integer; virtual; stdcall;
    function    _Release: integer; virtual; stdcall;
  end;

  TTweenElement = class(TTweenInterfacedObject, ITweenElement)
  {$IFDEF USE_STRICT}strict{$ENDIF}
  private
    FId:          string;
    FStartTime:   TDateTime;
    FStartValue:  double;
    FDistance:    double;
    FDuration:    double;
    FEasing:      TTweenEasingType;
    FBehavior:    TTweenBehavior;
    FEngine:      TTweenEngine;
    FState:       TTweenState;
    FValue:       double;
    FTag:         integer;
    FTagObj:      TObject;

    FOnPaused:    TTweenItemPausedEvent;
    FOnStarted:   TTweenItemStartedEvent;
    FOnResumed:   TTweenItemResumedEvent;
    FOnUpdated:   TTweenItemUpdatedEvent;
    FOnFinished:  TTweenItemFinishedEvent;

    // Implements:: ITweenElement
    // These give direct access to the values
    procedure   __Update(const NewValue: double); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetStartTime(const NewStartTime: TDateTime); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetStartValue(const NewStartValue: double); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetDistance(const NewDistance: double); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetDuration(const NewDuration: double); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetEasing(const NewEasing: TTweenEasingType); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetBehavior(const NewBehavior: TTweenBehavior); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetState(const NewState: TTweenState); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   __SetValue(const NewValue: double); {$IFDEF USE_INLINE}inline;{$ENDIF}

  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    // These are setter methods, values are checked before they
    // are assigned. These make up the public access layer
    procedure   SetStartTime(const NewStartTime: TDateTime); virtual;
    procedure   SetStartValue(const NewStartValue: double); virtual;
    procedure   SetDistance(const NewDistance: double); virtual;
    procedure   SetDuration(const NewDuration: double); virtual;
    procedure   SetEasing(const NewEasing: TTweenEasingType); virtual;
    procedure   SetBehavior(const NewBehavior: TTweenBehavior); virtual;
  public
    property    Engine: TTweenEngine read FEngine;

    function    Expired: boolean; {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   ResetData; {$IFDEF USE_INLINE}inline;{$ENDIF}

    property    Id: string read FId write FId;
    property    StartTime: TDateTime read FStartTime write SetStartTime;
    property    StartValue: double read FStartValue write SetStartValue;
    property    Distance: double read FDistance write SetDistance;
    property    Duration: double read FDuration write SetDuration;
    property    Easing: TTweenEasingType read FEasing write SetEasing;
    property    Behavior: TTweenBehavior read FBehavior write SetBehavior;
    property    Value: double read FValue;
    property    State: TTweenState read FState;

    property    Tag: integer read FTag write FTag;
    property    TagObject: TObject read FTagObj write FTagObj;

    constructor Create(const AOwner: TTweenEngine); virtual;
    destructor  Destroy; override;

  published
    property    OnStarted: TTweenItemStartedEvent read FOnStarted write FOnStarted;
    property    OnFinished: TTweenItemFinishedEvent read FOnFinished write FOnFinished;
    property    OnPaused: TTweenItemPausedEvent read FOnPaused write FOnPaused;
    property    OnResumed: TTweenItemResumedEvent read FOnResumed write FOnResumed;
    property    OnUpdated: TTweenItemUpdatedEvent read FOnUpdated write FOnUpdated;
  end;

  TTweenEngine = class(TObject)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FObjects:         TObjectList<TTweenElement>;
    FActive:          boolean;
    FPartial:         boolean;
    FInterval:        integer;
    FSyncRefresh:     boolean;
    FIgnoreOscillate: boolean;
    FUpdating:        integer;

    FOnPartial:       TTweenFinishedPartialEvent;
    FOnupdated:       TTweenUpdatedEvent;
    FOnFinished:      TTweenFinishedEvent;
    FOnStarted:       TTweenStartedEvent;

    FQRLUT: array[TTweenEasingType] of TTweenEasingFunction;

  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure   HandleSyncUpdate;
    function    HandleRepeatUpdate: boolean;
    function    UpdateTweenElement(const Item: TTweenElement): double;

    // These are connected to the BeginUpdate()
    // and EndUpdate() methods. This is to avoid changes
    // causing conflict inside the engine. You can safely override
    // UpdateBegins() and UpdateEnds() to know when the update-sequence
    // for all tweens begins, and when it ends.
    function    GetUpdating: boolean; {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   UpdateBegins; virtual;
    procedure   UpdateEnds; virtual;

    procedure   SetInterval(const Value: integer);
    function    GetElement(const Index: integer): TTweenElement;
    function    GetItemCount: integer;

    procedure   TweenStarted(const Item: TTweenElement); virtual;
    procedure   TweenComplete(const Item: TTweenElement); virtual;
    procedure   TweenPaused(const Item: TTweenElement); virtual;
    procedure   TweenResumed(const Item: TTweenElement); virtual;

    function    Linear(t,b,c,d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuadIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuadOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuadInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    CubeIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    CubeOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    CubeInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuartIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuartOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuartInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuintIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuintOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    QuintInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    SineIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    SineOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    SineInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    ExpoIn(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    ExpoOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}
    function    ExpoInOut(t, b, c, d: double): double; {$IFDEF USE_INLINE}inline;{$ENDIF}

  public

    function    ObjectOf(TweenId: string): TTweenElement;
    function    IndexOf(TweenId: string): integer;

    function    Add(TweenId: string): TTweenElement; overload;
    function    Add(TweenId: string; const StartValue, Distance, Duration: double;
                const AnimationType: TTweenEasingType;
                const Behavior: TTweenBehavior): TTweenElement; overload;
    function    Add(const Instance: TTweenElement): TTweenElement; overload;

    procedure   Delete(const Index: integer); overload; {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   Delete(TweenId: string); overload; {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   Delete(TweenIds: array of string); overload;

    procedure   Clear; overload;

    procedure   Execute; overload;
    procedure   Execute(const Finished: TTweenFinishedEvent); overload;
    procedure   Execute(const TweenObjects: array of TTweenElement); overload;

    procedure   Pause(const Index: integer); overload;
    procedure   Pause(const Tween: TTweenElement); overload;
    procedure   Pause(const TweenObjects: array of TTweenElement); overload;
    procedure   Pause(const TweenIds: array of string); overload;
    procedure   Pause; overload;

    procedure   Resume(const Index: integer); overload;
    procedure   Resume(const Tween: TTweenElement); overload;
    procedure   Resume(const TweenObjects: array of TTweenElement); overload;
    procedure   Resume(const TweenIds: array of string); overload;
    procedure   Resume;overload;

    procedure   BeginUpdate; {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   EndUpdate; {$IFDEF USE_INLINE}inline;{$ENDIF}

    procedure   Cancel; overload;

    class function GetTimeCode: double;

    property  Active: boolean read FActive;
    property  Updating: boolean read GetUpdating;
    property  Item[const index: integer]: TTweenElement read GetElement;
    property  Tween[Id: string]: TTweenElement read ObjectOf; default;
    property  Count: integer read GetItemCount;
    property  Interval: integer read FInterval write SetInterval;
    property  SyncRefresh: boolean read FSyncRefresh write FSyncRefresh;
    property  IgnoreOscillate: boolean read FIgnoreOscillate write FIgnoreOscillate;

    constructor Create;virtual;
    destructor Destroy;Override;

  public
    property OnPartialFinished: TTweenFinishedPartialEvent read FOnPartial write FOnPartial;
    property OnUpdated:   TTweenUpdatedEvent read FOnUpdated write FOnUpdated;
    property OnFinished:  TTweenFinishedEvent read FOnFinished write FOnFinished;
    property OnStarted:   TTweenStartedEvent read FOnStarted write FOnStarted;
  end;

  function DurationToTimeCode(const Duration: double): double;

  function TweenEngine: TTweenEngine;

implementation

uses
  qtx.system.dispatch;

var
  _Engine: TTweenEngine;

function TweenEngine: TTweenEngine;
begin
  if _Engine = nil then
    _Engine := TTweenEngine.Create;
  result := _Engine;
end;

function DurationToTimeCode(const Duration: double): double;
const
  MsPerSecond = 1000;
begin
  result := Duration * msPerSecond;
end;

//############################################################################
// Exception Error Messages
//############################################################################

Resourcestring
CNT_TWEEN_EXEC_EXISTS_ID =
'Execute failed, a tween-object with id [%s] already exists error';

CNT_TWEEN_EXEC_FAILED_ID =
'Execute failed, element does not have a valid id error';

CNT_TWEEN_EXEC_BUSY =
'Execute failed, tween already active error';

CNT_ERR_TWEEN_ELEMENT_EMPTY_ID =
'Failed to add tween-element, invalid [empty] id error';

CNT_ERR_TWEEN_ELEMENT_DUPLICATE_ID =
'Failed to add tween-element, [%s] already exists in the collection error';

CNT_ERR_TWEEN_ELEMENT_EXISTS =
'Failed to add tween-element [%s], element already exists in collection error';

CNT_ERR_TWEEN_BEHAVIOR_ACTIVE =
'Behavior cannot be altered while active';

CNT_ERR_TWEEN_DISTANCE_ACTIVE =
'Distance cannot be altered while active';

CNT_ERR_TWEEN_DURATION_INVALID =
'Invalid duration error. Value must be a positive';

CNT_ERR_TWEEN_DURATION_ACTIVE =
'Duration cannot be altered while active';

CNT_ERR_TWEEN_EASING_ACTIVE =
'Easing cannot be altered while active';

CNT_ERR_TWEEN_STARTTIME_ACTIVE =
'Start-time cannot be altered while active';

CNT_ERR_TWEEN_STARTVALUE_ACTIVE =
'Start-value cannot be altered while active';

CNT_ERR_TWEEN_FAILEDADD_NIL =
'Cannot add tween element, instance was nil error';


//############################################################################
// TTweenInterfacedObject
//############################################################################

function TTweenInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
begin
  If GetInterface(IID,Obj) then
    result := S_OK
  else
    result := E_NOINTERFACE;
end;

function TTweenInterfacedObject._AddRef: integer; stdcall;
begin
  // Shortcircuit reference counting safely
  result := -1;
end;

function TTweenInterfacedObject._Release: integer; stdcall;
begin
  // Shortcircuit reference counting safely
  result := -1;
end;

//############################################################################
// TTweenEngine
//############################################################################

constructor TTweenEngine.Create;
begin
  inherited Create;
  FObjects := TObjectList<TTweenElement>.Create(True);
  FInterval := 10;
  IgnoreOscillate := true;
  SyncRefresh := false;

  // Build Lookup-Table for faster access
  FQRLUT[ttlinear]      := Linear;
  FQRLUT[ttQuadIn]      := QuadIn;
  FQRLUT[ttQuadOut]     := QuadOut;
  FQRLUT[ttQuadInOut]   := QuadInOut;
  FQRLUT[ttCubeIn]      := CubeIn;
  FQRLUT[ttCubeOut]     := CubeOut;
  FQRLUT[ttCubeInOut]   := CubeInOut;
  FQRLUT[ttQuartIn]     := QuartIn;
  FQRLUT[ttQuartOut]    := QuartOut;
  FQRLUT[ttQuartInOut]  := QuartInOut;
  FQRLUT[ttQuintIn]     := QuintIn;
  FQRLUT[ttQuintOut]    := QuintOut;
  FQRLUT[ttQuintInOut]  := QuintInOut;
  FQRLUT[ttSineIn]      := SineIn;
  FQRLUT[ttSineOut]     := SineOut;
  FQRLUT[ttSineInOut]   := SineInOut;
  FQRLUT[ttExpoIn]      := ExpoIn;
  FQRLUT[ttExpoOut]     := ExpoOut;
  FQRLUT[ttExpoInOut]   := ExpoInOut;
end;

destructor TTweenEngine.Destroy;
begin
  if FActive then
    Cancel();

  Clear();
  FObjects.Free;
  inherited;
end;

procedure TTweenEngine.Clear;
begin
  // Cancel all active tweens
  if Active then
    Cancel();

  // Flush all tween objects
  FObjects.Clear();
end;

function TTweenEngine.GetElement(const Index: integer): TTweenElement;
begin
  result := FObjects[index];
end;

function TTweenEngine.GetItemCount: integer;
begin
  result := FObjects.Count;
end;

class function TTweenEngine.GetTimeCode: double;
begin
  result := MilliSecondsBetween(now, EncodeDateTime(1970, 01, 01, 0, 0, 0, 0) );
end;

procedure TTweenEngine.TweenResumed(const Item: TTweenElement);
begin
  if assigned(Item.OnResumed) then
    Item.OnResumed(Item);
end;

procedure TTweenEngine.SetInterval(Const Value: integer);
begin
  FInterval := EnsureRange(value, 1, MAXINT);
end;

procedure TTweenEngine.TweenPaused(const Item: TTweenElement);
begin
  if assigned(Item.OnPaused) then
    Item.OnPaused(Item);
end;

procedure TTweenEngine.TweenComplete(const Item:TTweenElement);
begin
  // Fire event for indivisual object
  if assigned(Item.OnFinished) then
    Item.OnFinished(Item);

  // Wait, was this the last object to finish?
  // If so, we should shut down the process
  if FObjects.Count = 1 then
    Cancel();
end;

procedure TTweenEngine.TweenStarted(const Item:TTweenElement);
begin
  if assigned(Item.OnStarted) then
    Item.OnStarted(Item);
end;

function TTweenEngine.UpdateTweenElement(const Item: TTweenElement): double;
var
  LTotal: double;
  LClDuration: double;
  LAccess: ITweenElement;
begin
  if Item.GetInterface(ITweenElement, LAccess) then
  begin
    LTotal := 0.0;
    LCLDuration := DurationToTimeCode(Item.Duration);
    //LCLDuration := Item.Duration;

    if not Item.Expired then
    begin
      LTotal := FQRLUT[item.Easing]( GetTimeCode() -Item.StartTime,
                        Item.StartValue,
                        Item.Distance,
                        LCLDuration);
    end else
    begin
      if Item.behavior = tbSingle then
      begin
        LTotal := Item.StartValue + Item.Distance;
      end else
      if Item.behavior = tbRepeat then
      begin
        LAccess.__SetStartTime( GetTimeCode() );
        LTotal := FQRLUT[item.Easing](GetTimeCode() - Item.StartTime,
                          Item.StartValue,
                          Item.Distance,
                          LCLDuration);
      end else
      if Item.behavior = tbOscillate then
      begin
        LAccess.__SetStartValue( Item.StartValue + Item.Distance );
        LAccess.__SetDistance( -Item.Distance );
        LAccess.__SetStartTime( GetTimeCode() );
        LTotal := FQRLUT[item.Easing](GetTimeCode() - Item.StartTime,
                          Item.StartValue,
                          Item.Distance,
                          LCLDuration);
        LAccess.__SetState(tsDone);
      end;
    end;

    if LTotal <> 0 then
      result := Round( LTotal * 100) / 100
    else
      result := 0.0;
  end else
    result := 0.0;
end;

function TTweenEngine.HandleRepeatUpdate: boolean;
var
  LItem:    TTweenElement;
  LAccess:  ITweenElement;
  LCount:   integer;
  LDone:    integer;
begin
  if not FActive then
  begin
    result := false;
    exit;
  end;

  // Tween objects cleared while active?
  if FObjects.Count < 1 then
  begin
    result := false;
    exit;
  end;

  BeginUpdate();
  try

    LCount := 0;
    LDone  := 0;

    for LItem in FObjects do
    begin
      if not FActive then
        exit(false);

      if LItem.GetInterface(ITweenElement, LAccess) then
      begin

        // The start time-code is set in the first update
        if LItem.State = tsIdle then
        begin
          LAccess.__SetState(tsRunning);
          LAccess.__SetStartTime( GetTimeCode() );
          TweenStarted(LItem);
        end;

        // Animation paused? Continue with next
        if LItem.State = tsPaused then
          continue;

        // Expired? Keep track of it
        if LItem.Expired() then
        begin
          if IgnoreOscillate then
          begin
            if (LItem.Behavior = tbSingle) then
              inc(LCount)
            else
              inc(LDone);
          end else
            inc(LCount);
        end;

        // Update the element with the new value
        // This differs from __SetValue() in that __Update()
        // fires an event, which delegates the change to
        // high-level components and code
        LAccess.__Update( UpdateTweenElement(Litem) );

        // Has it expired in terms of time after this update?
        if LItem.Expired() then
        begin
          // Already marked as done?
          if Litem.State <> tsDone then
          begin
            // Nope, ok mark it and perform completion
            LAccess.__SetState(tsDone);
            TweenComplete(LItem);
          end;
        end;
      end;
    end;

    // Fire our Updated event [if attached], this fires when
    // all tweens are done. Individual tween objects have their
    // own event handlers [see SetupAnimation() in the tween.components unit]
    if assigned(OnUpdated) then
      OnUpdated(Self);

    if LCount = (FObjects.Count - LDone) then
    begin
      if IgnoreOscillate then
      begin
        if not FPartial then
        begin
          // Make sure we only report once that a tween has
          // finished, which means the collection of tweens is partially
          // done. Each element also has its own events so this is just
          // a bonus event
          FPartial := true;
          if assigned(OnPartialFinished) then
            OnPartialFinished(self);
        end;
      end;
    end;
  finally
    EndUpdate();
  end;

  // Return true if there are still tweens that needs to finish
  result := LCount <> FObjects.Count;

  // All done? Ok, this is the place to end the animation sequence
  if not result then
    Cancel();
end;

procedure TTweenEngine.HandleSyncUpdate;
begin
  if not HandleRepeatUpdate() then
    Cancel()
  else
    TW3Dispatch.Execute(
      procedure ()
      begin
        HandleSyncUpdate();
      end,
      Interval
    );
end;

procedure TTweenEngine.Execute(const TweenObjects: array of TTweenElement);
var
  LItem:  TTweenElement;
  LId:    String;
begin

  if length(TweenObjects)>0 then
  begin
    for LItem in TweenObjects do
    begin
      LId := LItem.Id.Trim().ToLower();
      if LId.length > 0 then
      begin
        if IndexOf(LId) < 0 then
        begin
          LItem.Id := LId;
          FObjects.add(LItem);
        end
        else
          raise ETweenError.CreateFmt(CNT_TWEEN_EXEC_EXISTS_ID,[LId]);
      end else
        raise ETweenError.Create(CNT_TWEEN_EXEC_FAILED_ID);
    end;
  end;

  Execute();
end;

procedure TTweenEngine.Execute(const Finished: TTweenFinishedEvent);
begin
  // Check if callback is valid
  if assigned(Finished) then
    FOnFinished := Finished;

  Execute();
end;

procedure TTweenEngine.Execute;
begin
  if not FActive then
  begin

    FPartial := false;
    FActive := true;

    case SyncRefresh of
    true:   TW3Dispatch.Execute( HandleSyncUpdate, Interval);
    false:  TW3Dispatch.RepeatExecuteEx( HandleRepeatUpdate, interval, -1);
    end;

    if assigned(OnStarted) then
      OnStarted(self);

  end else
  raise ETweenError.Create(CNT_TWEEN_EXEC_BUSY);
end;

procedure TTweenEngine.Delete(TweenIds: array of string);
var
  x: integer;
  Lid:  string;
  LObj: TTweenElement;
  LIndex: Integer;
  FWasActive: boolean;
begin
  if length(tweenIds) > 0 then
  begin

    FWasActive := FActive;

    // Only remove tweens defined in Parameter list
    for x:= low(TweenIds) to high(TweenIds) do
    begin
      LId := TweenIds[x].Trim().ToLower();
      if LId.Length > 0 then
      begin
        LObj := ObjectOf(Lid);
        if LObj <> nil then
        begin
          LIndex := FObjects.IndexOf(LObj);
          if LIndex >= 0 then
            FObjects.Delete(LIndex);
        end;
      end;
    end;

    // Were we active before the above code?
    if FWasActive then
    begin
      // No tween elements left now?
      if FObjects.Count < 1 then
      begin
        // Ok, reset state then
        FActive := false;
        if assigned(FOnFinished) then
          FOnFinished(self);
      end;
    end;

  end;
end;

procedure TTweenEngine.Resume(const index: integer);
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if (Index >= 0) and (Index < FObjects.Count) then
    begin
      LObj := FObjects[index];
      if LObj.State = tsPaused then
      begin
        if LObj.GetInterface(ITweenElement, LAccess) then
        begin
          LAccess.__SetStartTime( GetTimeCode() );
          LAccess.__SetState( tsRunning );
          TweenResumed(LObj);
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Resume(const Tween: TTweenElement);
var
  LAccess: ITweenElement;
begin
  if Active then
  Begin
    if Tween <> nil then
    Begin
      if FObjects.IndexOf(Tween) >= 0 then
      begin
        if Tween.state = tsPaused then
        begin
          if Tween.GetInterface(ITweenElement, LAccess) then
          begin
            LAccess.__SetStartTime( GetTimeCode() );
            LAccess.__SetState( tsRunning );
            TweenResumed(Tween);
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Resume(const TweenObjects: array of TTweenElement);
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if length(TweenObjects) > 0 then
    Begin
      for LObj in TweenObjects do
      begin
        if LObj <> nil then
        begin
          if LObj.State = tsPaused then
          begin
            if LObj.GetInterface(ITweenElement, LAccess) then
            begin
              LAccess.__SetStartTime( GetTimeCode() );
              LAccess.__SetState( tsRunning );
              TweenResumed(LObj);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Resume(const TweenIds: array of string);
var
  x:  integer;
  LId:  string;
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if length(TweenIds) > 0 then
    begin
      for x := low(TweenIds) to high(TweenIds) do
      begin
        LId := TweenIds[x].Trim().ToLower();
        LObj := ObjectOf(Lid);
        if LObj <> nil then
        begin
          if LObj.State = tsPaused then
          begin
            if LObj.GetInterface(ITweenElement, LAccess) then
            begin
              LAccess.__SetStartTime( GetTimeCode() );
              LAccess.__SetState( tsRunning );
              TweenResumed(LObj);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Resume;
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if FObjects.Count > 0 then
    Begin
      for LObj in FObjects do
      begin
        if LObj.state = tsPaused then
        begin
          if LObj.GetInterface(ITweenElement, LAccess) then
          begin
            LAccess.__SetStartTime( GetTimeCode() );
            LAccess.__SetState( tsRunning );
            TweenResumed(LObj);
          end;
        end;
      end;
    end;
  end;
end;

Procedure TTweenEngine.Pause(const Index: integer);
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if (Index>=0) and (Index < FObjects.Count) then
    begin
      LObj := FObjects[index];
      if LObj.State=tsRunning then
      begin
        if LObj.GetInterface(ITweenElement, LAccess) then
        begin
          LAccess.__SetState( tsPaused );
          TweenPaused(LObj);
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Pause(const Tween: TTweenElement);
var
  LAccess: ITweenElement;
begin
  if Active then
  Begin
    if Tween<>NIL then
    Begin
      if FObjects.IndexOf(Tween) >= 0 then
      begin
        if Tween.state = tsRunning then
        begin
          if Tween.GetInterface(ITweenElement, LAccess) then
          begin
            LAccess.__SetState( tsPaused );
            TweenPaused(Tween);
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Pause(const TweenObjects: array of TTweenElement);
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if length(TweenObjects) >0 then
    Begin
      for LObj in TweenObjects do
      begin
        if LObj <> nil then
        begin
          if LObj.State = tsRunning then
          begin
            if LObj.GetInterface(ITweenElement, LAccess) then
            begin
              LAccess.__SetState( tsPaused );
              TweenPaused(LObj);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Pause(const TweenIds: array of String);
var
  x:  integer;
  LId:  string;
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if length(TweenIds) > 0 then
    begin
      for x := low(TweenIds) to high(TweenIds) do
      begin
        LId := TweenIds[x].Trim().ToLower();
        LObj := ObjectOf(Lid);
        if LObj <> nil then
        begin
          if LObj.State = tsPaused then
          begin
            if LObj.GetInterface(ITweenElement, LAccess) then
            begin
              LAccess.__SetState( tsPaused );
              TweenPaused(LObj);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TTweenEngine.Pause;
var
  LObj: TTweenElement;
  LAccess: ITweenElement;
begin
  if Active then
  begin
    if FObjects.Count > 0 then
    Begin
      for LObj in FObjects do
      begin
        if LObj.State = tsRunning then
        begin
          if LObj.GetInterface(ITweenElement, LAccess) then
          begin
            LAccess.__SetState( tsPaused );
            TweenPaused(LObj);
          end;
        end;
      end;
    end;
  end;
end;

function TTweenEngine.GetUpdating: boolean;
begin
  result := FUpdating > 0;
end;

procedure TTweenEngine.BeginUpdate;
begin
  inc(FUpdating);
  if FUpdating = 1 then
    UpdateBegins();
end;

procedure TTweenEngine.EndUpdate;
begin
  if FUpdating > 0 then
  begin
    dec(FUpdating);
    if FUpdating = 0 then
      UpdateEnds();
  end;
end;

procedure TTweenEngine.UpdateBegins;
begin
end;

procedure TTweenEngine.UpdateEnds;
begin
end;

procedure TTweenEngine.Cancel;
var
  el: TTweenElement;
  LAccess: ITweenElement;
begin
  if FActive then
  begin
    try
      // Set active to false
      FActive := false;

      // Make sure each tween is marked as done
      for el in FObjects do
      begin
        if el.State <> tsDone then
        begin
          if el.GetInterface(ITweenElement, LAccess) then
            LAccess.__SetState(tsDone);
        end;
      end;

    finally
      // Eccentrically important that this fires (!)
      if assigned(OnFinished) then
        OnFinished(self);
    end;
  end;
end;

Procedure TTweenEngine.Delete(const index: Integer);
var
  FWasActive: boolean;
begin
  if FObjects.Count > 0 then
  begin
    if (Index >= 0) and (Index < FObjects.Count) then
    begin
      // One animation? Just flush it and be done
      if FObjects.Count = 1 then
      begin
        Cancel();
        exit;
      end;

      // Remember if we are active while
      // this operation performs
      FWasActive := FActive;

      // Remove the tween element in question
      FObjects.Delete(Index);

      // Were we active before delete operation?
      if FWasActive then
      begin
        // If so, there used to be elements.
        // Has this changed? If so, execute the standard cleanup operation
        if FObjects.Count <= 0 then
          Cancel();
      end;

    end else
    raise ETweenError.CreateFmt
      ('Failed to delete tween element, expected index %d..%d not %d',
      [0, FObjects.Count-1, Index]);
  end;
end;

procedure TTweenEngine.Delete(TweenId: string);
begin
  Delete( FObjects.indexOf(ObjectOf(TweenId)) );
end;

function TTweenEngine.Add(const Instance: TTweenElement): TTweenElement;
begin
  result := Instance;
  if Instance <> nil then
  begin
    if FObjects.IndexOf(Instance) < 0 then
    begin
      Instance.Id := Instance.Id.trim().ToLower();
      if Instance.Id.length > 0 then
      begin
        if IndexOf(Instance.id) < 0 then
          FObjects.Add(Instance)
        else
          raise ETweenError.CreateFmt(CNT_ERR_TWEEN_ELEMENT_DUPLICATE_ID,[Instance.Id]);
      end else
      raise ETweenError.Create(CNT_ERR_TWEEN_ELEMENT_EMPTY_ID);
    end else
    raise ETweenError.CreateFmt(CNT_ERR_TWEEN_ELEMENT_EXISTS,[Instance.Id]);
  end else
  raise ETweenError.Create(CNT_ERR_TWEEN_FAILEDADD_NIL);
end;

function TTweenEngine.Add(TweenId: string): TTweenElement;
begin
  TweenId := TweenId.trim().ToLower();
  if TweenId.length > 0 then
  begin
    if IndexOf(TweenId)<0 then
    begin
      result := TTweenElement.Create(self);
      result.Id := TweenId;
      FObjects.add(result);
    end else
    raise ETweenError.CreateFmt(CNT_ERR_TWEEN_ELEMENT_DUPLICATE_ID,[TweenId]);
  end else
  raise ETweenError.Create(CNT_ERR_TWEEN_ELEMENT_EMPTY_ID);
end;

Function TTweenEngine.Add(TweenId: string;
          const StartValue, Distance, Duration: double;
          const AnimationType: TTweenEasingType;
          const Behavior: TTweenBehavior): TTweenElement;
begin
  TweenId := TweenId.trim().ToLower();
  if TweenId.length>0 then
  begin
    if IndexOf(TweenId) < 0 then
    begin
      result := TTweenElement.Create(self);
      result.StartValue := StartValue;
      result.Distance := Distance;
      result.Duration := Duration;
      result.Easing := Animationtype;
      result.Behavior := Behavior;
      result.StartTime := GetTimeCode();
      result.Id := TweenId;
      FObjects.add(result);
    end else
    raise ETweenError.CreateFmt(CNT_ERR_TWEEN_ELEMENT_DUPLICATE_ID,[TweenId]);
  end else
  raise ETweenError.Create(CNT_ERR_TWEEN_ELEMENT_EMPTY_ID);
end;

function TTweenEngine.IndexOf(TweenId: string): integer;
var
  x:  integer;
begin
  result := -1;
  TweenId := TweenId.Trim().ToLower();
  if TweenId.Length > 0 then
  begin
    for x := 0 to FObjects.Count-1 do
    begin
      if FObjects[x].Id = TweenId then
      begin
        result := x;
        break;
      end;
    end;
  end;
end;

function TTweenEngine.ObjectOf(TweenId: string): TTweenElement;
var
  x:  integer;
begin
  result := nil;
  TweenId := TweenId.Trim().ToLower();
  if TweenId.Length > 0 then
  begin
    for x := 0 to FObjects.Count-1 do
    begin
      if FObjects[x].Id = TweenId then
      begin
        result := FObjects[x];
        break;
      end;
    end;
  end;
end;

{$HINTS OFF}
function TTweenEngine.ExpoIn(t, b, c, d: double): double;
begin
  result := c * power(2,10 * (t/d-1))+b;
end;

function TTweenEngine.ExpoOut(t, b, c, d: double): double;
begin
  result := c * (-power(2,-10 * t/d)+1)+b;
end;

function TTweenEngine.ExpoInOut(t, b, c, d: double): double;
begin
  t := t / ( d / 2);
  if t < 1 then
    result := c/2 * power(2,10 * (t-1)) + b
  else
  begin
    t:=t-1;
    result := c/2 * (power(2,-10*t)+2)+b;
  end;
end;

function TTweenEngine.SineIn(t, b, c, d: double): double;
begin
  result := -c * cos(t/d * (PI / 2)) + c + b;
end;

function TTweenEngine.SineOut(t, b, c, d: double): double;
begin
  result := c * sin( t/d * (PI / 2) ) + b;
end;

function TTweenEngine.SineInOut(t, b, c, d: double):double;
begin
  result := -c / 2 * (cos( PI * t / d) -1) + b;
end;

function TTweenEngine.QuintIn(t, b, c, d: double): double;
begin
  t := t / d;
  result := c*t*t*t*t*t+b
end;

function TTweenEngine.QuintOut(t, b, c, d: double): double;
begin
  t := t / d;
  t := t -1;
  result := c * (t*t*t*t*t+1) + b;
end;

function TTweenEngine.QuintInOut(t, b, c, d: double): double;
begin
  t := t / (d / 2);
  if t < 1 then
    result := c/2 * t * t * t * t * t + b
  else
  begin
    t:=t-2;
    result := c/2 * (t*t*t*t*t+2) +b;
  end;
end;

function TTweenEngine.QuartIn(t, b, c, d: double): double;
begin
  t := t / d;
  result := c * t * t * t * t + b;
end;

function TTweenEngine.QuartOut(t, b, c, d: double): double;
begin
  t := t / d;
  t := t - 1;
  result := -c * (t * t * t * t - 1) + b;
end;

function TTweenEngine.QuartInOut(t, b, c, d: double): double;
begin
  t := t / (d/2);
  if t < 1 then
    result := c/2 * t * t * t * t + b
  else
  begin
    t := t - 2;
    result := -c / 2 * (t * t * t * t - 2) + b;
  end;
end;

function TTweenEngine.CubeInOut(t, b, c, d: double): double;
begin
  t := t / (d / 2);
  if t < 1 then
    result := c / 2 * (t * t * t) + b
  else
  begin
    t := t -2;
    result := c/2 * (t*t*t + 2) + b;
  end;
end;

function TTweenEngine.CubeOut(t, b, c, d: double): double;
begin
  t := t / d;
  t := t - 1;
  result := c * (t * t * t + 1) + b;
end;

function TTweenEngine.CubeIn(t, b, c, d: double): double;
begin
  t := t / d;
  result := c * t * t * t + b;
end;

function TTweenEngine.QuadInOut(t, b, c, d: double): double;
begin
  t := t / (d / 2);
  if (t < 1) then
    result := c / 2 * t * t + b
  else
  begin
    t := t -1;
    result := -c / 2 * (t * (t - 2) -1) + b;
  end;
end;

function TTweenEngine.QuadOut(t, b, c, d: double): double;
begin
  t := t / d;
  result := -c * t * (t - 2) + b;
end;

function TTweenEngine.QuadIn(t, b, c, d: double): double;
begin
  t := t / d;
  result := c * t * t + b;
end;

function TTweenEngine.Linear(t,b,c,d: double): double;
begin
  result := c * t / d + b;
end;
{$HINTS ON}

//############################################################################
// TTweenElement
//############################################################################

constructor TTweenElement.Create(const AOwner: TTweenEngine);
begin
  inherited Create();
  FEngine := AOwner;
  ResetData();
end;

destructor TTweenElement.Destroy;
begin
  // Just in case there are still calls made during
  // destruction
  FState := tsDone;
  FBehavior := tbSingle;
  inherited;
end;

function TTweenElement.Expired: Boolean;
begin
  //result := MillisecondsBetween(StartTime, Now) < FEngine.GetTimeCode
  //result := StartTime + Duration < TTweenEngine.GetTimeCode()
  result := StartTime + DurationToTimeCode(Duration) < TTweenEngine.GetTimeCode();
end;

Procedure TTweenElement.ResetData;
begin
  FStartTime := 0;
  FStartValue := 0;
  FDistance := 1.0;
  FDuration := 0.5;
  FValue := 0.0;
  FEasing := ttlinear;
  FBehavior := tbSingle;
  FState := tsIdle;
end;

// Called via interface access from engine
procedure TTweenElement.__SetStartTime(const NewStartTime: TDateTime);
begin
  FStartTime := NewStartTime;
end;

// Called via interface access from engine
procedure TTweenElement.__SetStartValue(const NewStartValue: double);
begin
  FStartValue := NewStartValue;
end;

// Called via interface access from engine
procedure TTweenElement.__SetDistance(const NewDistance: double);
begin
  FDistance := NewDistance
end;

// Called via interface access from engine
procedure TTweenElement.__SetDuration(const NewDuration: double);
begin
  FDuration := NewDuration
end;

// Called via interface access from engine
procedure TTweenElement.__SetEasing(const NewEasing: TTweenEasingType);
begin
  FEasing := NewEasing;
end;

procedure TTweenElement.__SetBehavior(const NewBehavior: TTweenBehavior);
begin
  FBehavior := NewBehavior;
end;

// Called via interface access from engine
procedure TTweenElement.__SetState(const NewState: TTweenState);
begin
  FState := NewState;
end;

// Set initial value, no event
procedure TTweenElement.__SetValue(const NewValue: double);
begin
  FValue := NewValue;
end;

// Update the value, fire event
procedure TTweenElement.__Update(const NewValue: double);
begin
  FValue := NewValue;

  if assigned(OnUpdated) then
    OnUpdated(self);
end;

// Property writer, applies checking
procedure TTweenElement.SetBehavior(const NewBehavior: TTweenBehavior);
begin
  if FState in [tsIdle, tsDone] then
    FBehavior := NewBehavior
  else
    raise ETweenElement.Create(CNT_ERR_TWEEN_BEHAVIOR_ACTIVE);
end;

// Property writer, applies checking
procedure TTweenElement.SetDistance(const NewDistance: double);
begin
  if FState in [tsIdle, tsDone] then
    FDistance := NewDistance
  else
    raise ETweenElement.Create(CNT_ERR_TWEEN_DISTANCE_ACTIVE);
end;

// Property writer, applies checking
procedure TTweenElement.SetDuration(const NewDuration: double);
begin
  if FState in [tsIdle, tsDone] then
  begin
    if NewDuration > 0.0 then
      FDuration := NewDuration
    else
      raise ETweenElement.Create(CNT_ERR_TWEEN_DURATION_INVALID);
  end else
    raise ETweenElement.Create(CNT_ERR_TWEEN_DURATION_ACTIVE);
end;

// Property writer, applies checking
procedure TTweenElement.SetEasing(const NewEasing: TTweenEasingType);
begin
  if FState in [tsIdle, tsDone] then
    FEasing := NewEasing
  else
    raise ETweenElement.Create(CNT_ERR_TWEEN_EASING_ACTIVE);
end;

// Property writer, applies checking
procedure TTweenElement.SetStartTime(const NewStartTime: TDateTime);
begin
  if FState in [tsIdle, tsDone] then
    FStartTime := NewStartTime
  else
    raise ETweenElement.Create(CNT_ERR_TWEEN_STARTTIME_ACTIVE);
end;

// Property writer, applies checking
procedure TTweenElement.SetStartValue(const NewStartValue: double);
begin
  if FState in [tsIdle, tsDone] then
    FStartValue := NewStartValue
  else
    raise ETweenElement.Create(CNT_ERR_TWEEN_STARTVALUE_ACTIVE);
end;

//############################################################################
// TTweenElement
//############################################################################

procedure ShutdownEngine;
begin
  if _Engine <> nil then
  begin
    try
      try
        if _Engine.Active then
          _Engine.Cancel();
      finally
        _Engine.Free();
      end;
    except
      // Sink exception, nothing we can do since the
      // module is being unloaded from memory. The actual exception
      // is picked up by the debugger and the callstack pinpoints
      // the culprit
      on e: exception do;
    end;
  end;
end;

initialization
begin
  _Engine := nil;
end;

finalization
begin
  ShutdownEngine();
end;

end.
