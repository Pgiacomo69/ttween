object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Tweening library ~ By Jon Lennart Aasenden'
  ClientHeight = 446
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 558
    Height = 50
    ButtonHeight = 44
    ButtonWidth = 81
    Caption = 'ToolBar1'
    DoubleBuffered = False
    EdgeBorders = [ebBottom]
    Images = ImageList1
    ParentDoubleBuffered = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Caption = 'Start Animation'
    end
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 106
    Width = 53
    Height = 337
    Align = alLeft
    Caption = 'Panel1'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    DesignSize = (
      53
      337)
    object BitBtn1: TBitBtn
      Left = 3
      Top = 3
      Width = 47
      Height = 34
      Action = acHAnim
      Anchors = [akTop, akRight]
      Caption = '<>'
      TabOrder = 0
      TabStop = False
    end
  end
  object PageControl1: TPageControl
    AlignWithMargins = True
    Left = 62
    Top = 106
    Width = 499
    Height = 337
    ActivePage = TabSheet1
    Align = alClient
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'First page'
      DoubleBuffered = True
      ParentDoubleBuffered = False
      object Button1: TButton
        Left = 112
        Top = 52
        Width = 225
        Height = 149
        Caption = 'Button1'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Second page'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 59
    Width = 558
    Height = 41
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 3
    DesignSize = (
      558
      41)
    object BitBtn2: TBitBtn
      Left = 504
      Top = 3
      Width = 47
      Height = 34
      Action = acVAnim
      Anchors = [akTop, akRight]
      Caption = '<>'
      TabOrder = 0
      TabStop = False
    end
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 452
    Top = 112
    object acHAnim: TAction
      Caption = 'Start Animation'
      OnExecute = acHAnimExecute
      OnUpdate = acHAnimUpdate
    end
    object acVAnim: TAction
      Caption = '<>'
      OnExecute = acVAnimExecute
      OnUpdate = acVAnimUpdate
    end
  end
  object ImageList1: TImageList
    ColorDepth = cd32Bit
    DrawingStyle = dsTransparent
    Height = 24
    Width = 24
    Left = 392
    Top = 60
  end
end
