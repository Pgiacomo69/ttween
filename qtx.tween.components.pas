unit qtx.tween.components;

// ############################################################################
// #
// # Quartex code library
// # ====================
// # Written by Jon L. Aasenden
// # Copyright Quartex Components LTD. All rights reserved.
// #
// # NOTE: This unit is not freeware. It has been released under Patreon,
// # which means that unless you have PAID A VALID TIER for this code,
// # you cannot use it in your projects.
// #
// # To obtain a valid support tier, please visit my Patreon page at:
// #    https://www.patreon.com/quartexnow
// #
// # Note: A valid Patreon tier for this code is very reasonable, ranging from
// #       USD $5 to $50. Please respect the time and effort that has gone
// #       into writing this source-code.
// #
// ############################################################################


interface

{$I 'qtx.tween.inc'}

uses
  System.Types,
  System.SysUtils,
  System.Classes,
  System.DateUtils,
  System.Generics.Collections,

  Vcl.Controls,
  Vcl.Forms,

  qtx.tween.core;

const
  CNT_STD_DELAY = 0.5;

type

  // Exceptions
  ETweenAnimation = class(ETweenError);
  ETweenHelper    = class(ETweenError);

  TTweenAnimation       = class;
  TTweenAnimAncestor    = TComponent;
  TTweenControlTarget   = TControl;

  [weak] TAnimationStartCB = reference to procedure(const Animation: TTweenAnimation);
  [weak] TAnimationEndsCB  = reference to procedure(const Animation: TTweenAnimation);

  TTweenAnimation = class(TTweenAnimAncestor)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FActive:    boolean;
    FControl:   TTweenControlTarget;
    FEngine:    TTweenEngine;
    FOnBegins:  TNotifyEvent;
    FOnEnds:    TNotifyEvent;
    FBehavior:  TTweenBehavior;
    FEndCB:     TAnimationEndsCB;
    FStartCB:   TAnimationStartCB;
    procedure   SetControl(const NewControl: TTweenControlTarget);
    procedure   SetActive(const NewActive: boolean); {$IFDEF USE_INLINE}inline;{$ENDIF}
    procedure   HandleAnimationBegins(Sender: TObject);
    procedure   HandleAnimationEnds(Sender: TObject);
  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure   SetupAnimation; virtual;
    procedure   CleanupAnimation; virtual;
  public
    property    Engine: TTweenEngine read FEngine;
    procedure   Loaded; override;

    procedure   InvokeCallback;

    procedure   Execute(const Finished: TAnimationEndsCB); overload;
    procedure   Execute(const Starts: TAnimationStartCB; const Finished: TAnimationEndsCB); overload;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property    Active: boolean read FActive write SetActive;
    property    Control: TTweenControlTarget read FControl write SetControl;
    property    Behavior: TTweenBehavior read FBehavior write FBehavior;

    property    OnAnimationBegins: TNotifyEvent read FOnBegins write FOnBegins;
    property    OnAnimationEnds: TNotifyEvent read FOnEnds write FOnEnds;
  end;

  TTweenSizeOptions = set of (
    soWidth,
    soHeight
  );

  ITweenDetail = interface
    ['{D53C6458-A231-469B-8937-7443EBC77FFA}']
    procedure SetInitialDetail(const Value: double);
  end;

  TTweenSizeDetails = class(TTweenInterfacedObject, ITweenDetail)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FInitialSize: double;
    FDuration:    double;
    FEasing:      TTweenEasingType;
    FSize:        double;

  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure SetInitialDetail(const Value: double);

  public
    property  InitialSize: double read FInitialSize;
    procedure Assign(Source: TPersistent); override;
  published
    property  Duration: double read FDuration write FDuration;
    property  Easing: TTweenEasingType read FEasing write FEasing;
    property  Size: double read FSize write FSize;
  end;

  TTweenSizeAnimation = class(TTweenAnimation)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FWidthDetails: TTweenSizeDetails;
    FHeightDetails: TTweenSizeDetails;
    FOptions:   TTWeenSizeOptions;
    procedure   SetWidth(const NewWidth: TTweenSizeDetails);
    procedure   SetHeight(const NewHeight: TTweenSizeDetails);
  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure   SetupAnimation; override;
    procedure   HandleSizeUpdated(const Sender: TTweenElement);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property    Options: TTWeenSizeOptions read FOptions write FOptions;
    property    Width: TTweenSizeDetails read FWidthDetails write SetWidth;
    property    Height: TTweenSizeDetails read FHeightDetails write SetHeight;
  end;

  TTweenMoveOptions = set of (soLeft, soTop);

  TTweenMoveDetails = class(TTweenInterfacedObject, ITweenDetail)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FInitialPos:double;
    FDuration:  double;
    FEasing:    TTweenEasingType;
    FPosition:  double;
  protected
    procedure   SetInitialDetail(const Value: double);
  public
    property    InitialPosition: double read FInitialPos;
    procedure   Assign(Source: TPersistent); override;
  published
    property    Duration: double read FDuration write FDuration;
    property    Easing: TTweenEasingType read FEasing write FEasing;
    property    Position: double read FPosition write FPosition;
    constructor Create; virtual;
  end;

  TTweenMoveAnimation = class(TTweenAnimation)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FLeftDetails: TTweenMoveDetails;
    FTopDetails:  TTweenMoveDetails;
    FOptions:     TTweenMoveOptions;
    procedure     SetLeft(const NewLeft: TTweenMoveDetails);
    procedure     SetTop(const NewTop: TTweenMoveDetails);
  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure     SetupAnimation; override;
    procedure     HandlePosUpdated(const Sender: TTweenElement);
  public
    constructor   Create(AOwner: TComponent); override;
    destructor    Destroy; override;
  published
    property      Options: TTweenMoveOptions read FOptions write FOptions;
    property      Left: TTweenMoveDetails read FLeftDetails write SetLeft;
    property      Top: TTweenMoveDetails read FTopDetails write SetTop;
  end;

  TTweenMoveSizeOptions = set of (msLeft, msTop, msWidth, msHeight);

  TTweenMoveSizeAnimation = class(TTweenAnimation)
  {$IFDEF USE_STRICT} strict {$ENDIF}
  private
    FOptions:   TTweenMoveSizeOptions;
    FLeft:      TTweenMoveDetails;
    FTop:       TTweenMoveDetails;
    FWidth:     TTweenSizeDetails;
    FHeight:    TTweenSizeDetails;
    procedure   SetLeft(const NewLeft: TTweenMoveDetails);
    procedure   SetTop(const NewTop: TTweenMoveDetails);
    procedure   SetWidth(const NewWidth: TTweenSizeDetails);
    procedure   SetHeight(const NewHeight: TTweenSizeDetails);

  {$IFDEF USE_STRICT} strict {$ENDIF}
  protected
    procedure   SetupAnimation; override;
    procedure   HandleUpdated(const Sender: TTweenElement);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property    Left: TTweenMoveDetails read FLeft write SetLeft;
    property    Top: TTweenMoveDetails read FTop write SetTop;
    property    Width: TTweenSizeDetails read FWidth write SetWidth;
    property    Height: TTweenSizeDetails read FHeight write SetHeight;
    property    Options: TTweenMoveSizeOptions read FOptions write FOptions;
  end;

  [weak] TTweenCallback = reference to procedure();

  TTweenHelper = class helper for TControl
  public
    procedure SizeRight(const NewWidth: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeRight(const NewWidth: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeRight(const NewWidth: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback ); overload;

    procedure SizeLeft(const NewWidth: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeLeft(const NewWidth: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeLeft(const NewWidth: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure SizeBottom(const NewHeight: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeBottom(const NewHeight: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeBottom(const NewHeight: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure SizeTop(const NewHeight: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeTop(const NewHeight: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeTop(const NewHeight: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;


    procedure SizeEvenly(const NewWidth, NewHeight: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeEvenly(const NewWidth, NewHeight: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure SizeEvenly(const NewWidth, NewHeight: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure MoveToRight(const NewLeft: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveToRight(const NewLeft: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveToRight(const NewLeft: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure MoveDownTo(const NewTop: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveDownTo(const NewTop: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveDownTo(const NewTop: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure GrowBy(const XFactor, YFactor: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure GrowBy(const XFactor, YFactor: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure GrowBy(const XFactor, YFactor: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback); overload;

    procedure ShrinkBy(const XFactor, YFactor: integer; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure ShrinkBy(const XFactor, YFactor: integer; const Duration: double; const Easing: TTweenEasingType = ttSineInOut); overload;
    procedure ShrinkBy(const XFactor, YFactor: integer; const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback ); overload;
  end;

  // mirror helper for TGraphicControl
  TTweenHelperGfx = class helper(TTweenHelper) for TGraphicControl
  end;

  // mirror helper for TWinControl
  TTweenHelperWin = class helper(TTweenHelper) for TWinControl
  end;

  // mirror helper for TCustomControl and derived classes
  TTweenHelperCustom = class helper(TTweenHelper) for TCustomControl
  end;

  // In the v1.2 edition I am planning to use proper queues for each
  // control that is animated. This means that when you fire an animation on
  // a control, a queue object is created for that control.
  // The queue remains in memory until the control is released.
  // When the control is released the Notification() method fires on the
  // TTweenControlQueueManager component - and the queue is released and
  // notifications removed.
  TTweenControlQueue = class(TObject)
  strict private
    FControl:   TControl;
    FStack:     TStack<TTweenAnimation>;
  strict protected
    function    GetCount: integer;
    function    GetEmpty: boolean;
  public
    property    Control: TControl read FControl;
    property    Empty: boolean read GetEmpty;
    property    Count: integer read GetCount;

    function    Contains(const Animation: TTweenAnimation): boolean;
    procedure   Push(const Animation: TTweenAnimation);
    function    Pop: TTweenAnimation;

    procedure   Clear;

    constructor Create(const Control: TControl); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  // This is the queue manager, it is written as a component because we
  // want to be notified whenever a control is destroyed. When that happens
  // the queue for that control (see TTweenControlQueue comments) is released.
  // Such queues are created on demand whenever you run an Animation on a control.
  //
  // The reason we need queues is that we want a clean, linear list of waiting
  // animations. At the moment any other animations you run on a control
  // at the same time, is just waiting to geta lock. This means that if you
  // run 3 animations on a control, animation #3 could fire before Animation #2.
  //
  // A proper queue would ensure that all animations line up in the right order,
  // and also that we can cancel ALL animations in transit.
  // There would be no need for procedures like TweenLockControl() to mark
  // a control as "busy".
  //
  TTweenControlQueueManager = class(TComponent)
  strict private
    FTable:     TDictionary<TControl, TTweenControlQueue>;
  protected
    procedure   Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    function    InitalizeQueueFor(const Control: TControl): TTweenControlQueue;
    procedure   FinalizeQueueFor(const Control: TControl);
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

  procedure TweenLockControl(const Control: TTweenControlTarget);
  procedure TweenUnLockControl(const Control: TTweenControlTarget);
  function  TweenControlLocked(const Control: TTweenControlTarget): boolean;


implementation

var
  _ControlLUT:  TList<TComponent>;


resourcestring
  CNT_ERR_ITweenDetail =
  'Internal error, failed to obtain %s::ITweenDetail interface error';


// This procedure is used to keep track of a control that is presently
// being animated by a tween control. The purpose is to avoid having
// multiple tween animations targeting the same control at once.
// We simply keep the control in a list during animation, and remove
// it from the list once the animation has finished (see TweenUnLockControl)
procedure TweenLockControl(const Control: TTweenControlTarget);
begin
  if _ControlLut <> nil then
  begin
    if Control <> nil then
    begin
      if _ControlLut.IndexOf(Control) < 0 then
        _ControlLUT.Add(Control)
      {$IFDEF USE_EXCEPTION_ON_LOCK}
      else
        raise ETweenAnimation.CreateFmt
          ('Control [%s] already marked as busy error', [Control.Name]);
      {$ELSE}
      ;
      {$ENDIF}
    end else
      raise ETweenAnimation.Create
      ('Failed to lock control, reference was nil error');
  end;
end;

// This procedure removes a control from the list we use to avoid
// multiple tweens affecting the same control. Once an animation has run
// its course, this procedure is called, opening up the control
// for other animations (or the same animation).
procedure TweenUnLockControl(const Control: TTweenControlTarget);
var
  LIndex: integer;
begin
  if _ControlLut <> nil then
  begin
    if Control <> nil then
    begin
      LIndex := _ControlLut.IndexOf(Control);
      if LIndex >=0 then
        _ControlLut.Delete(LIndex)
      {$IFDEF USE_EXCEPTION_ON_LOCK}
      else
        raise ETweenAnimation.CreateFmt
          ('Control [%s] not marked as busy', [Control.Name]);
      {$ELSE}
        ;
      {$ENDIF}
    end else
      raise ETweenAnimation.Create
      ('Failed to unlock control, reference was nil error');
  end;
end;

// This function checks if a control is busy with an animation
// and returns true or false. This is a function called by the
// high-level animation classes before doing much of anything
function TweenControlLocked(const Control: TTweenControlTarget): boolean;
begin
  if _ControlLut <> nil then
  begin
    if Control <> nil then
      result := _ControlLut.IndexOf(Control) >= 0
    else
      raise ETweenAnimation.Create
      ('Failed to query tween lock state, control was nil error');
  end else
    result := false;
end;

//############################################################################
// TTweenControlQueueManager
//############################################################################

constructor TTweenControlQueueManager.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FTable := TDictionary<TControl, TTweenControlQueue>.Create;
end;

destructor TTweenControlQueueManager.Destroy;
begin
  FTable.Free;
  inherited;
end;

function TTweenControlQueueManager.InitalizeQueueFor(const Control: TControl): TTweenControlQueue;
begin
  result := nil;
  if Control <> nil then
  begin
    if not FTable.ContainsKey(Control) then
    begin
      // Create a queue for the control
      result := TTweenControlQueue.Create(Control);
      FTable.Add(Control, result);

      // yes we want to be notified when the control is destroyed
      Control.FreeNotification(self);

    end else
      result := FTable[Control];
  end;
end;

procedure TTweenControlQueueManager.FinalizeQueueFor(const Control: TControl);
var
  LStack: TTweenControlQueue;
begin
  if Control <> nil then
  begin
    if FTable.ContainsKey(Control) then
    begin
      Control.RemoveFreeNotification(self);

      LStack := nil;
      try
        LStack := FTable[Control];
        FTable.Remove(Control);
      finally
        if LStack <> nil then
          LStack.Free;
      end;

    end;
  end;
end;

procedure TTweenControlQueueManager.Notification(AComponent: TComponent; Operation: TOperation);
begin
  if AComponent <> nil then
  begin
    if AComponent <> self then
    begin
      if (AComponent is TControl) then
      begin
        case Operation of
        opRemove: FinalizeQueueFor(TControl(AComponent));
        opInsert: inherited Notification(AComponent, Operation);
        end;
      end else
        inherited Notification(AComponent, Operation);
    end else
      inherited Notification(AComponent, Operation);
  end else
    inherited Notification(AComponent, Operation);
end;

//############################################################################
// TTweenControlQueue
//############################################################################

constructor TTweenControlQueue.Create(const Control: TControl);
begin
  inherited Create();
  FControl := Control;
end;

destructor TTweenControlQueue.Destroy;
begin
  if not Empty then
    Clear();
  inherited;
end;

procedure TTweenControlQueue.Clear;
var
  LObj: TTweenAnimation;
begin
  // Trigger callback
  for LObj in FStack do
  begin
    LObj.InvokeCallback();
  end;
end;

function TTweenControlQueue.GetCount: integer;
begin
  result := FStack.Count;
end;

function TTweenControlQueue.GetEmpty: boolean;
begin
  result := FStack.Count < 1;
end;

function TTweenControlQueue.Pop: TTweenAnimation;
begin
  result := FStack.Pop();
end;

function TTweenControlQueue.Contains(const Animation: TTweenAnimation): boolean;
var
  LTemp: TTweenAnimation;
begin
  result := false;
  if Animation <> nil then
  begin
    if FStack.Count > 0 then
    begin
      for LTemp in FStack do
      begin
        result := LTemp = Animation;
        if result then
          break;
      end;
    end;
  end;
end;

procedure TTweenControlQueue.Push(const Animation: TTweenAnimation);
begin
  if Animation <> nil then
  begin
    if not Contains(Animation) then
      FStack.Push(Animation)
    else
      raise ETweenAnimation.Create('Failed to add animation to queue, item already on stack');
  end else
    raise ETweenAnimation.Create('Failed to add animation to queue, item was NIL error');
end;

//############################################################################
// TTweenHelper
//############################################################################

procedure TTweenHelper.MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer;
  const Easing: TTweenEasingType = ttSineInOut);
var
  LAnimation: TTweenMoveSizeAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenMoveSizeAnimation.Create(nil);
  LAnimation.Options := [msLeft, msTop, msWidth, msHeight];
  LAnimation.Control := self;

  if LAnimation.Left.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.left);
    LAnimation.Left.Position := NewLeft;
    LAnimation.Left.Easing := Easing;
    LAnimation.Left.Duration := CNT_STD_DELAY;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['left']);

  if LAnimation.Top.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Top);
    LAnimation.Top.Position := NewTop;
    LAnimation.Top.Easing := Easing;
    LAnimation.Top.Duration := CNT_STD_DELAY;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['top']);

  if LAnimation.Width.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Width);
    LAnimation.Width.Size := NewWidth;
    LAnimation.Width.Easing := Easing;
    LAnimation.Width.Duration := CNT_STD_DELAY;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['width']);

  if LAnimation.Height.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Height);
    LAnimation.Height.Size := NewHeight;
    LAnimation.Height.Easing := Easing;
    LAnimation.Height.Duration := CNT_STD_DELAY;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['height']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    TThread.Queue(nil, procedure ()
    begin
      Animation.Free;
    end);
  end);
end;

procedure TTweenHelper.MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  MoveAndSize(NewLeft, NewTop, NewWidth, NewHeight, Duration, Easing, NIL);
end;

procedure TTweenHelper.MoveAndSize(const NewLeft, NewTop, NewWidth, NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LAnimation: TTweenMoveSizeAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenMoveSizeAnimation.Create(nil);
  LAnimation.Options := [msLeft, msTop, msWidth, msHeight];
  LAnimation.Control := self;

  if LAnimation.Left.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.left);
    LAnimation.Left.Position := NewLeft;
    LAnimation.Left.Easing := Easing;
    LAnimation.Left.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['left']);

  if LAnimation.Top.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Top);
    LAnimation.Top.Position := NewTop;
    LAnimation.Top.Easing := Easing;
    LAnimation.Top.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['top']);

  if LAnimation.Width.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Width);
    LAnimation.Width.Size := NewWidth;
    LAnimation.Width.Easing := Easing;
    LAnimation.Width.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['width']);

  if LAnimation.Height.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Height);
    LAnimation.Height.Size := NewHeight;
    LAnimation.Height.Easing := Easing;
    LAnimation.Height.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['height']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

procedure TTweenHelper.MoveDownTo(const NewTop: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  MoveDownTo(NewTop, CNT_STD_DELAY, Easing, NIL);
end;

procedure TTweenHelper.MoveDownTo(const NewTop: integer; const Duration: double;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  MoveDownTo(NewTop, Duration, Easing, NIL);
end;

procedure TTweenHelper.MoveDownTo(const NewTop: integer; const Duration: double;
  const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LAnimation: TTweenMoveAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenMoveAnimation.Create(nil);
  LAnimation.Options := [soTop];
  LAnimation.Control := self;

  if LAnimation.Top.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Top);
    LAnimation.Top.Position := NewTop;
    LAnimation.Top.Easing := Easing;
    LAnimation.Top.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['top']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

procedure TTweenHelper.MoveToRight(const NewLeft: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  MoveToRight(NewLeft, CNT_STD_DELAY, Easing, NIL);
end;

procedure TTweenHelper.MoveToRight(const NewLeft: integer; const Duration: double;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  MoveToRight(NewLeft, Duration, Easing, NIL);
end;

procedure TTweenHelper.MoveToRight(const NewLeft: integer; const Duration: double;
  const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LAnimation: TTweenMoveAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenMoveAnimation.Create(nil);
  LAnimation.Options := [soLeft];
  LAnimation.Control := self;

  if LAnimation.Left.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.left);
    LAnimation.Left.Position := NewLeft;
    LAnimation.Left.Easing := Easing;
    LAnimation.Left.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['left']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

procedure TTweenHelper.ShrinkBy(const XFactor, YFactor: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  ShrinkBy(XFactor, YFactor, CNT_STD_DELAY, Easing);
end;

procedure TTweenHelper.ShrinkBy(const XFactor, YFactor: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeEvenly(Width - XFactor, Height - YFactor, Duration, Easing, NIL);
end;

procedure TTweenHelper.ShrinkBy(const XFactor, YFactor: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
begin
  SizeEvenly(Width - XFactor, Height - YFactor, Duration, Easing, CB);
end;

procedure TTweenHelper.GrowBy(const XFactor, YFactor: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  GrowBy(XFactor, YFactor, CNT_STD_DELAY, Easing);
end;

procedure TTweenHelper.GrowBy(const XFactor, YFactor: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeEvenly(Width + XFactor, Height + YFactor, Duration, Easing);
end;

procedure TTweenHelper.GrowBy(const XFactor, YFactor: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
begin
  SizeEvenly(Width + XFactor, Height + YFactor, Duration, Easing, CB);
end;

procedure TTweenHelper.SizeEvenly(const NewWidth, NewHeight: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeEvenly(NewWidth, NewHeight, CNT_STD_DELAY, Easing);
end;

procedure TTweenHelper.SizeEvenly(const NewWidth, NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeEvenly(NewWidth, NewHeight, Duration, Easing, NIL);
end;

procedure TTweenHelper.SizeEvenly(const NewWidth, NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LDiffX, LDiffY: integer;
  dx, dy: integer;
begin
  if (NewWidth > 0) or (NewHeight > 0) then
  begin
    // Setup X factors
    if NewWidth > Self.Width then
    begin
      LDiffX := NewWidth - self.Width;
      if LDiffX > 1 then
        LDiffX := LDiffX div 2
      else
        LDiffX := 0;
      dx := self.Left - LDiffX;
    end else
    if NewWidth < Self.Width then
    begin
      LDiffX := self.Width - NewWidth;
      if LDiffX > 1 then
        LDiffX := LDiffX div 2
      else
        LDiffX := 0;
      dx := self.Left + LDiffX;
    end else
    dx := Self.Left;

    // Setup Y factors
    if NewHeight > Self.Height then
    begin
      LDiffY := NewHeight - self.Height;
      if LDiffY > 1 then
        LDiffY := LDiffY div 2
      else
        LDiffY := 0;
      dy := self.Top - LDiffY;
    end else
    if NewHeight < Self.Height then
    begin
      LDiffY := self.Height - NewHeight;
      if LDiffY > 1 then
        LDiffY := LDiffY div 2
      else
        LDiffY := 0;
      dy := self.Top + LDiffY;
    end else
    dy := self.Top;

    MoveAndSize(
      dx,
      dy,
      NewWidth,
      NewHeight,
      Duration,
      Easing,
      CB
    );
  end else
  begin
    if assigned(CB) then
      CB();
  end;
end;

procedure TTweenHelper.SizeTop(const NewHeight: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeTop(NewHeight, CNT_STD_DELAY, Easing, NIL);
end;

procedure TTweenHelper.SizeTop(const NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeTop(NewHeight, Duration, Easing, NIL);
end;

procedure TTweenHelper.SizeTop(const NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LAnimation: TTweenMoveSizeAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenMoveSizeAnimation.Create(nil);
  LAnimation.Options := [msTop, msHeight];
  LAnimation.Control := self;

  // We want to scale to the new size
  if LAnimation.Height.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Height);
    LAnimation.Height.Size := NewHeight;
    LAnimation.Height.Easing := Easing;
    LAnimation.Height.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['height']);

  // We also want to move to new Y pos, giving the illusion of
  // a bottom panel scaling down [for example]
  if LAnimation.Top.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Top);
    LAnimation.Top.Position := self.Top + (NewHeight - Self.height);
    LAnimation.Top.Easing := Easing;
    LAnimation.Height.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['top']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

procedure TTweenHelper.SizeBottom(const NewHeight: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeBottom(NewHeight, CNT_STD_DELAY, Easing, NIL);
end;

procedure TTweenHelper.SizeBottom(const NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeBottom(NewHeight, Duration, Easing, NIL);
end;

procedure TTweenHelper.SizeBottom(const NewHeight: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LAnimation: TTweenSizeAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenSizeAnimation.Create(nil);
  LAnimation.Options := [soHeight];
  LAnimation.Control := self;

  if LAnimation.Height.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Height);
    LAnimation.Height.Size := NewHeight;
    LAnimation.Height.Easing := Easing;
    LAnimation.Height.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['height']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

procedure TTweenHelper.SizeLeft(const NewWidth: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeLeft(NewWidth, CNT_STD_DELAY, Easing);
end;

procedure TTweenHelper.SizeLeft(const NewWidth: integer; const Duration: double;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeLeft(NewWidth, Duration, Easing, NIL);
end;

procedure TTweenHelper.SizeLeft(const NewWidth: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback);
var
  LDiff: integer;
  dx: integer;
begin
  if Width > NewWidth then
    LDiff := Self.Width - NewWidth
  else
    LDiff := NewWidth - self.Width;

  dx := Self.Left - LDiff;
  MoveAndSize(dx,Self.Top, NewWidth, Self.Height, Duration, Easing, CB);
end;

procedure TTweenHelper.SizeRight(const NewWidth: integer;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeRight(NewWidth, CNT_STD_DELAY, Easing, NIL);
end;

procedure TTweenHelper.SizeRight(const NewWidth: integer; const Duration: double;
  const Easing: TTweenEasingType = ttSineInOut);
begin
  SizeRight(NewWidth, Duration, Easing, NIL);
end;

procedure TTweenHelper.SizeRight(const NewWidth: integer;
  const Duration: double; const Easing: TTweenEasingType; const CB: TTweenCallback );
var
  LAnimation: TTweenSizeAnimation;
  LAccess: ITweenDetail;
begin
  LAnimation := TTweenSizeAnimation.Create(nil);
  LAnimation.Options := [soWidth];
  LAnimation.Control := self;

  if LAnimation.Width.GetInterface(ITweenDetail, LAccess) then
  begin
    LAccess.SetInitialDetail(self.Width);
    LAnimation.Width.Size := NewWidth;
    LAnimation.Width.Easing := Easing;
    LAnimation.Width.Duration := Duration;
  end else
    raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['width']);

  LAnimation.Execute( procedure (const Animation: TTweenAnimation)
  begin
    try
      TThread.Queue(nil, procedure ()
      begin
        Animation.Free;
      end);
    finally
      if assigned(CB) then
        CB();
    end;
  end);
end;

//############################################################################
// TTweenPositionDetails
//############################################################################

constructor TTweenMoveDetails.Create;
begin
  inherited Create;
  FInitialPos := 0.0;
  FDuration := CNT_STD_DELAY;
  FEasing := TTweenEasingType.ttlinear;
  FPosition := 0.0;
end;

procedure TTweenMoveDetails.Assign(Source: TPersistent);
begin
  if Source <> nil then
  begin
    if (source is TTweenMoveDetails) then
    begin
      FDuration := TTweenMoveDetails(Source).Duration;
      FEasing := TTweenMoveDetails(Source).Easing;
      FPosition := TTweenMoveDetails(Source).Position;
    end else
    inherited Assign(Source);
  end else
  inherited Assign(Source);
end;

procedure TTweenMoveDetails.SetInitialDetail(const Value: double);
begin
  FInitialPos := Value;
end;

//############################################################################
// TTweenDetails
//############################################################################

procedure TTweenSizeDetails.Assign(Source: TPersistent);
begin
  if source <> nil then
  begin
    if source is TTweenSizeDetails then
    begin
      FDuration := TTweenSizeDetails(Source).Duration;
      FEasing := TTweenSizeDetails(Source).Easing;
      FSize := TTweenSizeDetails(Source).Size;
    end else
    inherited Assign(Source);
  end else
  inherited Assign(Source);
end;

procedure TTweenSizeDetails.SetInitialDetail(const Value: double);
begin
  FInitialSize := Value;
end;

//############################################################################
// TTweenMoveSizeAnimation
//############################################################################

constructor TTweenMoveSizeAnimation.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FLeft := TTweenMoveDetails.Create;
  FLeft.Duration := CNT_STD_DELAY;
  FLeft.Easing := ttSineInOut;

  FTop := TTweenMoveDetails.Create;
  FTop.Duration := CNT_STD_DELAY;
  FTop.Easing := ttSineInOut;

  FWidth := TTweenSizeDetails.Create;
  FWidth.Duration := CNT_STD_DELAY;
  FWidth.Easing := ttSineInOut;

  FHeight := TTweenSizeDetails.Create;
  FHeight.Duration := CNT_STD_DELAY;
  FHeight.Easing := ttSineInOut;

  FOptions := [msLeft, msTop, msWidth, msHeight];

  Behavior := tbSingle;
end;

destructor TTweenMoveSizeAnimation.Destroy;
begin
  FLeft.free;
  FTop.Free;
  FWidth.Free;
  FHeight.Free;
  inherited;
end;

procedure TTweenMoveSizeAnimation.SetLeft(const NewLeft: TTweenMoveDetails);
begin
  FLeft.Assign(NewLeft);
end;

procedure TTweenMoveSizeAnimation.SetTop(const NewTop: TTweenMoveDetails);
begin
  FTop.Assign(NewTop);
end;

procedure TTweenMoveSizeAnimation.SetWidth(const NewWidth: TTweenSizeDetails);
begin
  FWidth.Assign(NewWidth);
end;

procedure TTweenMoveSizeAnimation.SetHeight(const NewHeight: TTweenSizeDetails);
begin
  FHeight.Assign(NewHeight);
end;

procedure TTweenMoveSizeAnimation.SetupAnimation;
var
  LItem: TTweenElement;
  LAccess: ITweenDetail;
begin
  inherited SetupAnimation();

  if (msLeft in FOptions) then
  begin
    if Left.GetInterface(ITweenDetail, LAccess) then
    begin
      //Left.SetInitialPosition(Control.Left);
      LAccess.SetInitialDetail(Control.Left);
      LItem := Engine.Add('left');
      LItem.Distance := Left.Position - Left.InitialPosition;
      LItem.Easing := Left.Easing;
      LItem.Duration := Left.Duration;
      LItem.OnUpdated := HandleUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 1;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Left']);
  end;

  if (msTop in FOptions) then
  begin
    if Top.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Top);
      LItem := Engine.Add('top');
      LItem.Distance := Top.Position - Top.InitialPosition;
      LItem.Easing := Top.Easing;
      LItem.Duration := Top.Duration;
      LItem.OnUpdated := HandleUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 2;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Top']);
  end;

  if (msWidth in FOptions) then
  begin
    if Width.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Width);
      LItem := Engine.Add('width');
      LItem.Distance := Width.Size - Width.InitialSize;
      LItem.Easing := Width.Easing;
      LItem.Duration := Width.Duration;
      LItem.OnUpdated := HandleUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 3;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Width']);
  end;

  if (msHeight in FOptions) then
  begin
    if Height.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Height);
      LItem := Engine.Add('height');
      LItem.Distance := Height.Size - Height.InitialSize;
      LItem.Easing := Height.Easing;
      LItem.Duration := Height.Duration;
      LItem.OnUpdated := HandleUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 4;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Height']);
  end;
end;

procedure TTweenMoveSizeAnimation.HandleUpdated(const Sender: TTweenElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    case Sender.Tag of
    1: Control.Left := round(Left.InitialPosition + Sender.Value);
    2: Control.Top := round(Top.InitialPosition + Sender.Value);
    3: Control.Width := round(Width.InitialSize + Sender.Value);
    4: Control.Height := round(Height.InitialSize + Sender.Value);
    end;
  end;
end;

//############################################################################
// TTweenMoveAnimation
//############################################################################

constructor TTweenMoveAnimation.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FLeftDetails := TTweenMoveDetails.Create;
  FLeftDetails.Duration := CNT_STD_DELAY;
  FLeftDetails.Easing := ttSineInOut;

  FTopDetails := TTweenMoveDetails.Create;
  FTopDetails.Duration := CNT_STD_DELAY;
  FTopDetails.Easing := ttSineInOut;

  FOptions := [soLeft, soTop];
  Behavior := tbSingle;
end;

destructor TTweenMoveAnimation.Destroy;
begin
  FLeftDetails.Free;
  FTopDetails.Free;
  inherited;
end;

procedure TTweenMoveAnimation.SetLeft(const NewLeft: TTweenMoveDetails);
begin
  FLeftDetails.Assign(NewLeft);
end;

procedure TTweenMoveAnimation.SetTop(const NewTop: TTweenMoveDetails);
begin
  FTopDetails.Assign(NewTop);
end;

procedure TTweenMoveAnimation.SetupAnimation;
var
  LItem: TTweenElement;
  LAccess: ITweenDetail;
begin
  inherited SetupAnimation();

  if (soLeft in FOptions) then
  begin
    if Left.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Left);
      LItem := Engine.Add('left');
      LItem.Distance := FLeftDetails.Position - FLeftDetails.InitialPosition;
      LItem.Easing := FLeftDetails.Easing;
      LItem.Duration := FLeftDetails.Duration;
      LItem.OnUpdated := HandlePosUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 1;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Left']);
  end;

  if (soTop in FOptions) then
  begin
    if Top.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Top);
      LItem := Engine.Add('top');
      LItem.Distance := FTopDetails.Position - FTopDetails.InitialPosition;
      LItem.Easing := FTopDetails.Easing;
      LItem.Duration := FTopDetails.Duration;
      LItem.OnUpdated := HandlePosUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 2;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Top']);
  end;
end;

procedure TTweenMoveAnimation.HandlePosUpdated(const Sender: TTweenElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    case Sender.Tag of
    1: Control.Left := round(FLeftDetails.InitialPosition + Sender.Value);
    2: Control.Top := round(FTopDetails.InitialPosition + Sender.Value);
    end;
  end;
end;

//############################################################################
// TTweenSizeAnimation
//############################################################################

constructor TTweenSizeAnimation.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidthDetails := TTweenSizeDetails.Create;
  FWidthDetails.Duration := CNT_STD_DELAY;
  FWidthDetails.Easing := ttSineInOut;

  FHeightDetails := TTweenSizeDetails.Create;
  FHeightDetails.Duration := CNT_STD_DELAY;
  FHeightDetails.Easing := ttSineInOut;

  FOptions := [soWidth, soHeight];
  Behavior := tbSingle;
end;

destructor TTweenSizeAnimation.Destroy;
begin
  FWidthDetails.free;
  FHeightDetails.free;
  inherited;
end;

procedure TTweenSizeAnimation.SetWidth(const NewWidth: TTweenSizeDetails);
begin
  FWidthDetails.Assign(NewWidth);
end;

procedure TTweenSizeAnimation.SetHeight(const NewHeight: TTweenSizeDetails);
begin
  FHeightDetails.Assign(NewHeight);
end;

procedure TTweenSizeAnimation.SetupAnimation;
var
  LItem: TTweenElement;
  LAccess: ITweenDetail;
begin
  inherited SetupAnimation();

  if (soWidth in FOptions) then
  begin
    if FWidthDetails.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Width);
      LItem := Engine.Add('width');
      LItem.Distance := FWidthDetails.Size - Control.Width;
      LItem.Easing := FWidthDetails.Easing;
      LItem.Duration := FWidthDetails.Duration;
      LItem.OnUpdated := HandleSizeUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 1;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Width']);
  end;

  if (soHeight in FOptions) then
  begin
    if FHeightDetails.GetInterface(ITweenDetail, LAccess) then
    begin
      LAccess.SetInitialDetail(Control.Height);
      LItem := Engine.Add('height');
      LItem.Distance := FHeightDetails.Size - Control.height;
      LItem.Easing := FHeightDetails.Easing;
      LItem.Duration := FHeightDetails.Duration;
      LItem.OnUpdated := HandleSizeUpdated;
      LItem.Behavior := Behavior;
      LItem.Tag := 2;
    end else
      raise ETweenHelper.CreateFmt(CNT_ERR_ITweenDetail, ['Height']);
  end;
end;

procedure TTweenSizeAnimation.HandleSizeUpdated(const Sender: TTweenElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    case Sender.Tag of
    1: Control.Width := round(FWidthDetails.InitialSize + Sender.Value);
    2: Control.Height := round(FHeightDetails.InitialSize + Sender.Value);
    end;
  end;
end;

//############################################################################
// TTweenAnimation
//############################################################################

constructor TTweenAnimation.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FEngine := TTweenEngine.Create();
  FEngine.OnFinished := HandleAnimationEnds;
  FEngine.OnStarted := HandleAnimationBegins;
end;

destructor TTweenAnimation.Destroy;
begin
  if FEngine.Active then
    FEngine.Cancel();

  FEngine.Free;
  inherited;
end;

procedure TTweenAnimation.Loaded;
begin
  inherited;

  // If active has been set during loading
  // the start the animation ASAP
  if FActive then
  begin
    FActive := false;
    SetActive(true);
  end;
end;

procedure TTweenAnimation.HandleAnimationBegins(Sender: TObject);
begin
  // Do the public event first
  if assigned(FOnBegins) then
    FOnBegins(Self);

  // Do callback second
  if assigned(FStartCB) then
    FStartCB(self);
end;

procedure TTweenAnimation.HandleAnimationEnds(Sender: TObject);
begin
  try
    FActive := false;
    CleanupAnimation();
  finally
    // Do the public event first
    if assigned(FOnEnds) then
      FOnEnds(self);

    // Do the callback second
    if assigned(FEndCB) then
      FEndCB(self);
  end;
end;

procedure TTweenAnimation.SetupAnimation;
begin
  TweenLockControl(FControl);
end;

procedure TTweenAnimation.CleanupAnimation;
begin
  TweenUnLockControl(FControl);
  FEngine.Clear();
end;

procedure TTweenAnimation.InvokeCallback;
begin
  if assigned(FEndCB) then
    FEndCB(self);
end;

procedure TTweenAnimation.Execute(const Starts: TAnimationStartCB;
  const Finished: TAnimationEndsCB);
begin
  if not (csDestroying in ComponentState) then
  begin
    if not Active then
    begin
      FStartCB := Starts;
      FEndCB := Finished;
      SetActive(True);
    end;
  end;
end;

procedure TTweenAnimation.Execute(const Finished: TAnimationEndsCB);
begin
  if not (csDestroying in ComponentState) then
  begin
    if not Active then
    begin
      FEndCB := Finished;
      SetActive(True);
    end;
  end;
end;

procedure TTweenAnimation.SetActive(const NewActive: boolean);
begin
  if (csLoading in ComponentState) then
    FActive := NewActive
  else
  begin
    if NewActive <> FActive then
    begin
      If FEngine.Active then
      begin
        FEngine.Cancel();
        FActive := false;
      end;

      // New state is to run? OK, do that then.
      // We would already have stopped it upstairs so
      // no need to check for a false positive
      if NewActive then
      begin
        // Make sure we have something to animate
        if not assigned(FControl) then
          raise ETweenAnimation.Create('No target control is set, please assign a visual control to animate');

        // And bombs away!
        SetupAnimation();
        FEngine.Execute();
      end;
    end;
  end;
end;

procedure TTweenAnimation.SetControl(const NewControl: TTweenControlTarget);
begin
  if (csLoading in ComponentState) then
    FControl := newControl
  else
  begin
    if not FActive then
      FControl := NewControl
    else
      raise ETweenAnimation.Create('Cannot change tween-control while active error');
  end;
end;

procedure SetupAnimationUnit;
begin
  _ControlLUT := TList<TComponent>.Create;
end;

procedure FinalizeAnimationUnit;
begin
  if _ControlLUT <> nil then
    FreeAndNIL(_ControlLUT);
end;

initialization
begin
  SetupAnimationUnit();
end;

finalization
begin
  FinalizeAnimationUnit();
end;


end.
