program tween_testbed;

uses
  Vcl.Forms,
  mainform in 'mainform.pas' {Form1},
  qtx.tween.components in 'qtx.tween.components.pas',
  qtx.tween.core in 'qtx.tween.core.pas',
  qtx.tween.package in 'qtx.tween.package.pas',
  qtx.system.dispatch in 'qtx.system.dispatch.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
