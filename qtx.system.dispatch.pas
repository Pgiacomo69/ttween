unit qtx.system.dispatch;

// ############################################################################
// #
// # Quartex code library
// # ====================
// # Written by Jon L. Aasenden
// # Copyright Quartex Components LTD. All rights reserved.
// #
// # NOTE: This unit is not freeware. It has been released under Patreon,
// # which means that unless you have PAID A VALID TIER for this code,
// # you cannot use it in your projects.
// #
// # To obtain a valid support tier, please visit my Patreon page at:
// #    https://www.patreon.com/quartexnow
// #
// # Note: A valid Patreon tier for this code is very reasonable, ranging from
// #       USD $5 to $50. Please respect the time and effort that has gone
// #       into writing this source-code.
// #
// ############################################################################

interface

uses
  System.Types,
  System.SysUtils,
  System.Classes,
  System.DateUtils,
  System.Math,
  System.Generics.Collections
  {$IFDEF MSWINDOWS}
  , Winapi.Windows
  , Vcl.Forms
  {$ENDIF}
  ;

type

  TProcedureRef         = reference to procedure;
  TProcedureObjRef      = procedure of object;
  TRepeatFunc           = reference to function (): boolean;
  TRepeatFuncObj        = reference to function (): boolean;

  EW3Dispatch = class(Exception);
  TW3Dispatch = class
  public
    class function QueryProgramEnded: boolean; {$IFDEF USE_INLINE}inline;{$ENDIF}

    // Anonymous method implementation
    class procedure Execute(const EntryPoint: TProcedureRef; const ms: Cardinal); overload;
    class function  ExecuteFn(const EntryPoint: TProcedureRef; const ms: Cardinal): THandle;
    class procedure AbortExecute(const aHandle: THandle);

    // Pass "rounds = -1" to repeat perpetually until entrypoint returns false
    class procedure RepeatExecute(const Entrypoint: TRepeatFunc; const ms: cardinal; Rounds: integer);
    class procedure RepeatExecuteEx(const EntryPoint: TRepeatFuncObj; const ms: cardinal; Rounds: integer);

    class function GetTickCount: Cardinal; static;

    // Object method implementation
    class procedure Delay(const EntryPoint: TProcedureRef; const ms: Cardinal); overload;
    class Procedure Delay(const EntryPoint: TProcedureObjRef; const ms: Cardinal); overload;
  end;


implementation

resourcestring
  CNT_ERR_DISPATCH_INVALID_DELAY =
  'Invalid delay factor, expected 1 or above error';


var
  {$IFDEF USE_THREADS}
  _LUT: TW3ThreadDictionary;
  _TED: integer;
  {$ELSE}
  _LUT: TDictionary<UINT, TProcedureRef>;
  {$ENDIF}

{$IFDEF USE_THREADS}
type
  TW3DispatchThread = class(TThread)
  strict private
    FCBAnon:    TThreadProcedure;
    FCBObj:     TProcedureObjRef;
    FDelay:     integer;
    FObjCall:   boolean;
    FId:        integer;
  protected
    procedure   DispatchToObject;
    procedure   Execute; override;
  public
    property Id: integer read FId write FId;
    constructor Create(ms: integer; Entrypoint: TThreadProcedure); overload;
    constructor Create(ms: integer; Entrypoint: TProcedureObjRef); overload;
  end;

  TW3ThreadDictionary = class(TDictionary<integer, TW3DispatchThread>)
  protected
    procedure ThreadFinished(Sender: TObject);
  end;
{$ENDIF}


//############################################################################
// TW3ThreadDictionary
//############################################################################

{$IFDEF USE_THREADS}
procedure TW3ThreadDictionary.ThreadFinished(Sender: TObject);
var
  LId: integer;
begin
  if sender <> nil then
  begin
    if self <> nil then
    begin
      LId := TW3DispatchThread(sender).Id;
      Self.Remove(LId);
    end;
  end;
end;
{$ENDIF}

//#############################################################################
// TW3DispatchThread
//#############################################################################

{$IFDEF USE_THREADS}
constructor TW3DispatchThread.Create(ms: integer; Entrypoint: TProcedureObjRef);
begin
  inherited Create(true);
  FreeOnTerminate := true;
  FCBObj := Entrypoint;
  FObjCall := true;
  FDelay := ms;
end;

constructor TW3DispatchThread.Create(ms: integer; Entrypoint: TThreadProcedure);
begin
  inherited Create(true);
  FreeOnTerminate := true;
  FCBAnon := Entrypoint;
  FObjCall := false;
  FDelay := ms;
end;

procedure TW3DispatchThread.DispatchToObject;
begin
  if not TW3Dispatch.QueryProgramEnded() then
    FCBObj();
end;

procedure TW3DispatchThread.Execute;
begin
  // Application killed before instance executed?
  if self = nil then
    exit;

  // Thread terminated prematurely?
  if Terminated then
    exit;

  // Alright! Let's do the Mambo #5 and, eh, sleep for a while.
  // Wow that was an anti climax wasnt it..
  repeat
    // Take a short vacation
    sleep(1);

    // Did we die in our sleep?
    if self = nil then
      exit;

    // Did our owner wet the bed?
    if Terminated then
      break;

    // Update delay factor & rewind
    dec(FDelay);
    if FDelay < 1 then
      break;
  until terminated;

  // Are we dead? .. again?
  if self = nil then
    exit;

  // No? Not even a bit miffed?
  if Terminated then
    exit;

  try
    case FObjCall of
    true:   Synchronize(DispatchToObject);
    false:
      // Whenever you have time darling
      if assigned(FCBAnon) then
        TThread.Queue(nil, FCBAnon);
    end;
  except
    // Only 3 factors can cause an exception here:
    // 1. Instance is killed during context switch
    // 2. Referenced method + module is unloaded from memory during context switch
    // 3. The user is an total bastard, from which is there is no cure
    // Either way, sink the exception. The callstack will show the user
    // why his code failed in the event handler
    on exception do;
  end;

end;
{$ENDIF}

//#############################################################################
// TW3Dispatch
//#############################################################################

class function TW3Dispatch.GetTickCount: Cardinal;
begin
  result := TThread.GetTickCount;
end;

class function TW3Dispatch.QueryProgramEnded: boolean;
begin
  if Application <> nil then
    result := Application.Terminated
  else
    result := true;
end;

class procedure TW3Dispatch.RepeatExecute(const Entrypoint: TRepeatFunc;
  const ms: cardinal; Rounds: integer);
begin
  if assigned(Entrypoint) then
  begin
    if (ms > 0) and (Rounds <> 0) then
    begin
      if not QueryProgramEnded then
      begin
        Execute( procedure ()
        begin
          if Entrypoint() then
          begin
            // Note: We allow "-1" as rounds, meaning perpetual
            if Rounds < 0 then
              RepeatExecute(Entrypoint, ms, Rounds)
            else
              RepeatExecute(Entrypoint, ms, Rounds -1);
          end;
        end, ms);
      end;
    end;
  end;
end;

class procedure TW3Dispatch.RepeatExecuteEx(const EntryPoint: TRepeatFuncObj;
  const ms: cardinal; Rounds: integer);
begin
  if assigned(Entrypoint) then
  begin
    if (ms > 0) and (Rounds <> 0) then
    begin
      if not QueryProgramEnded then
      begin
        Execute( procedure ()
        begin
          if Entrypoint() then
          begin
            // Note: We allow "-1" as rounds, meaning perpetual
            if Rounds < 0 then
              RepeatExecuteEx(Entrypoint, ms, Rounds)
            else
              RepeatExecuteEx(Entrypoint, ms, Rounds -1);
          end;
        end, ms);
      end;
    end;
  end;
end;

class procedure TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const ms: Cardinal);

  {$IFNDEF USE_THREADS}
  procedure w3_invoke(hwnd: HWND; uMsg: UINT;
            idEvent: UINT_PTR; dwTime: cardinal); stdcall;
  var
    LProc:  TProcedureRef;
  begin
    KillTimer(0,idEvent);
    try
      if assigned(_LUT) then
      begin
        LProc:=_LUT.Items[idEvent];
        _LUT.Remove(idEvent);
        if assigned(LProc) then
          LProc();
      end;
    except
      // Nothing we can do, since this can only happen if
      // the instance is prematurely terminated. Sink the exception
      // and allow TApplication to do it's thing
      on exception do;
    end;
  end;
  {$ENDIF}

{$IFDEF USE_THREADS}
var
  LThread: TW3DispatchThread;
{$ENDIF}
begin
  if Assigned(_LUT) then
  {$IFDEF USE_THREADS}
  begin
    // No need for atomic, all re-entries are synchronized
    inc(_TED);
    LThread := TW3DispatchThread.Create(ms, TThreadProcedure(EntryPoint));
    _LUT.Add(_TED, LThread);

    LThread.Id := _TED;
    LThread.OnTerminate := _LUT.ThreadFinished;

    LThread.Start();
  end;
  {$ELSE}
  _LUT.add( SetTimer(0, 0, ms, @w3_invoke), EntryPoint);
  {$ENDIF}
end;

class function TW3Dispatch.ExecuteFn(const EntryPoint: TProcedureRef; const ms: Cardinal): THandle;

 {$IFNDEF USE_THREADS}
  procedure w3_invoke(hwnd: HWND; uMsg: cardinal;
            idEvent: UIntPtr; dwTime: cardinal); stdcall;
  var
    LProc:  TProcedureRef;
  begin
    KillTimer(0,idEvent);
    try
      if _LUT <> nil then
      begin
        LProc := _LUT.Items[idEvent];
        _lut.Remove(idEvent);

        if assigned(LProc) then
          LProc();
      end;
    except
      on exception do;
    end;
  end;
  {$ENDIF}

{$IFDEF USE_THREADS}
var
  LThread: TW3DispatchThread;
{$ENDIF}
begin
  result := 0;
  if Assigned(_LUT) then
  begin
    {$IFDEF USE_THREADS}
    inc(_TED);
    result := _TED;

    LThread := TW3DispatchThread.Create(ms, TThreadProcedure(EntryPoint));
    LThread.FreeOnTerminate := true;
    LThread.Id := result;
    LThread.OnTerminate := _LUT.ThreadFinished;
    _LUT.Add(result, LThread);
    LThread.Start();
    {$ELSE}
    result := SetTimer(0, 0, ms, @w3_invoke);
    _LUT.Add(result, EntryPoint);
    {$ENDIF}
  end;
end;

class procedure TW3Dispatch.AbortExecute(const aHandle: THandle);
{$IFDEF USE_THREADS}
var
  LInstance: TW3DispatchThread;
{$ENDIF}
begin
  if _LUT <> nil then
  begin
    if _LUT.ContainsKey(aHandle) then
    begin
    {$IFDEF USE_THREADS}
      if _lut.TryGetValue(aHandle, LInstance) then
      begin
        if not LInstance.Terminated then
          LInstance.Terminate();
      end;
    {$ELSE}
      _LUT.Remove(aHandle);
      KillTimer(0, aHandle);
    {$ENDIF}
    end;
  end;
end;

class procedure TW3Dispatch.Delay(const EntryPoint: TProcedureRef; const ms: cardinal);
var
  LThen:  cardinal;
begin
  if not QueryProgramEnded() then
  begin
    if ms > 0 then
    begin
      try
        LThen := TW3Dispatch.GetTickCount() + ms;

        repeat
          Sleep(1);
          if QueryProgramEnded() then
            break;
        until ( GetTickCount >= LThen );

        if assigned(EntryPoint) then
        begin
          if QueryProgramEnded() then
            EntryPoint();
        end;
      except
        on exception do;
      end;
    end else
      raise EW3Dispatch.Create(CNT_ERR_DISPATCH_INVALID_DELAY);
  end;
end;

class procedure TW3Dispatch.Delay(const EntryPoint: TProcedureObjRef;
      const ms:Cardinal);
var
  LThen:  cardinal;
begin
  if not QueryProgramEnded() then
  begin
    if ms > 0 then
    begin
      try

        LThen := GetTickCount() + ms;

        repeat
          Sleep(1);

          if QueryProgramEnded() then
            break;
        until ( GetTickCount() >= LThen );

        if assigned(EntryPoint) then
        begin
          if not QueryProgramEnded() then
            EntryPoint();
        end;
      except
        on exception do;
      end;
    end else
      raise EW3Dispatch.Create(CNT_ERR_DISPATCH_INVALID_DELAY);
  end;
end;

//#############################################################################
// Unit procedures
//#############################################################################

procedure SetupDispatch;
begin
  {$IFDEF USE_THREADS}
  _TED := 0;
  _LUT := TW3ThreadDictionary.Create;
  {$ELSE}
  _LUT := TDictionary<UINT, TProcedureRef>.Create;
  {$ENDIF}
end;

procedure FinalizeDispatch;
begin
  if _LUT <> nil then
    _LUT.free;
end;


Initialization
begin
  SetupDispatch();
end;

finalization
begin
  FinalizeDispatch();
end;


end.
